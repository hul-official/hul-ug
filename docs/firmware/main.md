# Firmware

## Version.1系とVersion.3系の違い

HULのリリース当時から2021年7月まで保守を続けてきたファームウェア（version.1）とそれ以降のファームウェア（version.3）ではBCTの仕様変更に伴いソフトウェアに互換性がありません。
Version.2系はver.3系へアップデートする際の内部開発系統なので公開していません。
Version.1ではBCTでRBCP addressの一部をデータ線として使っていましたが、version.3ではRBCP address 32 bitとRBCP data 8 bitをそのまま利用するようにしています。
ソフトウェアでもModuleIDとLocalAddressによる表記をやめ、RBCP addressでローカルモジュールを指定するように変更しました。
ModuleIDやLocalAddressはlocal bus controllerの構造を知らないユーザーにとっては不必要な情報であるためです。

Version.3系では全てのファームウェアの共通機能として2つのローカルモジュールが追加されました。SiTCP経由でSPI flash memoryを書き換えるFlash Memory Programmer (FMP)、およびFPGAや基板の状態を自己診断するSelf Diagnosis System (SDS)です。FMPのおかげでUSBブラスターケーブルを使って書き込んでいたMCSファイルをネットワーク経由でダウンロードすることが出来るようになりました。これにより遠隔でのファームウェア更新が可能となりました。SDSは放射線によるソフトエラー修正回数の監視、および修復不可能な状態に落ちった事の検出が可能です。また、FPGAの温度、およびVCCINT、VCCAUX、VCCBRAM電位の監視が可能です。

## ファームウェアの基本構造
HULのファームウェアには大別するとデータ収集を担うTDCのようなタイプと、トリガー回路のようなデータをPCへ転送しないタイプが存在します。
後者は機能によってブロック構成が異なりますが、データ収集タイプのファームウェアは統一されたブロック構造を持ちます。
以下にデータ収集タイプのブロック図を示します。
図中オレンジで示した構成要素はどのファームウェアにも実装されている機能です。
各ローカルモジュールを制御するlocal busはlocal bus controller (BCT)によって制御されており、BCTはSiTCPのRBCP (UDP)と接続されています。
FMPとSDSは全てのversion.3系のファームウェアに実装されているローカルモジュールであり、機能は前述の通りです。

データ収集に関わる部分は図中の白い四角で表されています。
TDCやスケーラといった計測に関わるブロックは計測ブロックと呼ばれます。
それらの機能は主に*user circuit*と書かれたブロックに実装されます。
これら*user circuit*からのデータはbuilder busを介してevent builderブロックに集められ、イベントヘッダを付与した後SiTCPを通じてPCへ転送されます。
Receiver block、trigger manager、およびJ0 interfaceはトリガー入出力に関わるブロックです。
HULはMTM (GNN-570)から送信されてくるトリガーとイベントタグ情報を受け取るように設計されています。
Receiver blockはHRMメザニンカードが実装されている場合有効になるブロックであり、トリガーを上流の回路から受け取る役割を持ちます。
このブロックはトリガー受信と同時に受信したトリガー情報をPCへ転送するためにbuilder busにも接続されています。
Receiver blockを利用するHULはVMEクレート内のJ0バスマスタであると思われます。
J0 interfaceはHULがJ0バスマスタである場合、receiver blockが受け取ったトリガーとタグ情報をJ0バスへ流します。
一方J0バスに対してスレイブである場合、J0バスからやってきたトリガー情報をtrigger managerへ渡します。
また、HULは汎用モジュールであるため、MTMが存在しない状況でも利用できるようにNIMポートからのトリガー入力をサポートしています。
Trigger managerはファームウェア内部のトリガー配布を担っており、HRMメザニン (receiver block)、NIM入力、J0バス入力からトリガー入力経路を選択できます。
Trigger managerが送信する*LEVEL2*、*Fast clear*、*TAG*の情報はevent builder内部でデータ送信の決定やイベントヘッダ情報の生成に利用されます。


![daqtype-block](fw-overview.png "データ収集タイプファームウェアのブロック図"){: width=90% }

## 各ファームウェア共通の事柄

<span class="myred">hul_softwareは共通部分であるBCT、FMP、およびSDSのアドレスを`common/src/RegisterMapCommon.hh`で管理しています。</span>

### リセットシーケンス

2017年12月19日づけの更新分から各ファームウェアのリセットシーケンスを統一しました。
これまでのファームウェアではBCTがハングアップするとリセットする手段が電源OFF以外なかったり、MH-TDCではBCTリセットをかけても一部FIFOの状態がリセットされず、DAQがハングしている状態が解除されなかったり、という問題がありました。
そこで、リセットのレベルを段階ごとに設定して対処できるようにしました。

**リセット方法（下に行くほど影響範囲が広いです）**

- BCTリセット：BCTのResetにRBCPで書き込みを行う。BCT管理下にあるユーザーモジュールがリセットされる。通常のリセット。
- SiTCPリセット：`sitcp_controller.cc`のReset_SiTCPを呼び出す。BCTがハングしている場合でもリセット可能。BCTもデッドロック状態から復帰する。
- ハードリセット：基板上のSW3を押す。SiTCPを含む全回路がリセットされる。最も強制力がある。

通常はBCTリセットを利用し、操作を誤ってBCTがデッドロックした場合はSiTCPリセットを、SiTCPがハングしてしまった場合はハードリセットを、という順番で利用してください。

### サポートするネットワーク規格

現在公開しているファームウェアでは、SiTCPはGbEでしか動作しないようになっています。
SiTCP自体は100 MbpsとGbEの両方をサポートし自動切り替えが可能なのですが、利用しているPHYの都合で自動切り替えの機能を意図的に利用不可にしています。
100 Mbpsで利用する要求はほぼないだろうと思っているので100 Mbpsのバージョンは作成していません。
<span class="myred">100 Mbpsでしか通信できないネットワークスイッチにつなぐとSiTCPが動かなくなります。</span>

### Bus controller

BCTにはローカルモジュールにアクセスする機能のほかに、BCTリセットを発行する、バージョンを取得する、FPGAの再コンフィグを行う、という機能が実装されています。
これらの機能はRBCP addressで以下のアドレスを実行することで利用可能です。
BCTに対してVersion読み出しを行うと32bitのレジスタが返ってきます。
このうち上位16bitがFirmware固有名で、下位16bitがバージョン (メジャーバージョン 8bit + マイナーバージョン 8bit) となります。
以下の表においてビット幅が`-`と表記されているレジスタについては、送信レジスタ値の内容を気にしません。
書き込み動作を行うことで効力が発揮されます。

**RBCP address of BCT (Module ID: 0xE)**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-10">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
        <th>備考</th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Reset</td>
    <td class="tcenter">0xE000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Bus Controllerからモジュールリセット信号をアサートし、SiTCPを除く全モジュールを初期化。</td>
  </tr>
  <tr>
    <td>Version</td>
    <td class="tcenter">0xE010'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Firmwareの固有名とバージョンを読み出す。多バイト読み出しが必要。</td>
  </tr>
  <tr>
    <td>Reconfig</td>
    <td class="tcenter">0xE020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>PROG_B_ONをLowにしてFPGAの再コンフィギュレーションを行う。一度通信が切れるので暫くしてから再接続。</td>
  </tr>
</tbody>
</table>

### Flash Memory Programmer

FMPはPCから指定したSPIコマンドを送信することでメモリがサポートしているコマンドの実行、ページデータの読み書きが可能です。
メモリの消去、書き込み、読み出しをFMPを通じて行う事が可能であり、Vivadoを通じてメモリへ書き込む動作（消去、書き込み、ベリファイ）と同じことがネットワークを通じて実行可能です。

<span class="myred">FMPには既知のバグがあります。</span>ページ書き込みをする際に書き込みコマンドを実行してからそのコマンドが終了する前に次のページ書き込みを要求できてしまいます。
現状はファームウェア側で対処せずにソフトウェア側で十分な時間待って次のページ書き込みコマンドを送信するようにしていますが、あまり良い対処方法ではありません。
そのうちファームウェア側で根本対処するかもしれません。
このような対処を行っているため、ネットワークで書き込みを行っている割には時間がかかります。
書き込みに失敗する事象が頻発する場合、著者へ連絡をしてください。

以下にRBCPアドレスを記載しますが、`hul_software`に同梱されている`FlashMemoryProgrammer.cc`以外からFMPを制御するのはお勧めしません。
間違って書き込みロックビットを立ててしまうと、そのメモリには二度と書き込みが出来なくなります。

**RBCP address of FMP (Module ID: 0xD)**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-10">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
        <th>備考</th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Status</td>
    <td class="tcenter">0xD000'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>FMPモジュールのステータスビットを取得します。<br>
        現状下位1ビットにSPIコマンドサイクルbusyが割り当てられています。
    </td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0xD000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>SPIシーケンスのモード切替を行います。<br>
      1~2-bit目: SPIシーケンスモード
      <ul>
        <li>0x0: Read mode</li>
        <li>0x1: Write mode</li>
        <li>0x2: Instruction mode</li>
      </ul>
      3-bit目: Dummy mode <br>
      Chip selectをOFFにし、SPIシーケンスを回してもフラッシュメモリが反応しないようにします。
    </td>
  </tr>
  <tr>
    <td>Register</td>
    <td class="tcenter">0xD020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SPIコマンドのビット列を与えます。</td>
  </tr>
  <tr>
    <td>InstLength</td>
    <td class="tcenter">0xD030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>SPIコマンドのビット長を与えます。</td>
  </tr>
  <tr>
    <td>ReadLength</td>
    <td class="tcenter">0xD040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">10</td>
    <td>ページ読み出し長を与えます。</td>
  </tr>
  <tr>
    <td>WriteLength</td>
    <td class="tcenter">0xD050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">10</td>
    <td>ページ書き込み長を与えます。</td>
  </tr>
  <tr>
    <td>ReadCountFIFO</td>
    <td class="tcenter">0xD060'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">10</td>
    <td>ページ読み出ししたデータが格納されているFIFOのread countを取得します。</td>
  </tr>
  <tr>
    <td>ReadFIFO</td>
    <td class="tcenter">0xD070'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>8-bitずつページ読み出しFIFOからデータ読み出します。</td>
  </tr>
  <tr>
    <td>WriteCountFIFO</td>
    <td class="tcenter">0xD080'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">10</td>
    <td>ページ書き込み用のデータを格納するFIFOのwrite countを取得します。</td>
  </tr>
  <tr>
    <td>WriteFIFO</td>
    <td class="tcenter">0xD090'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">8</td>
    <td>8-bitずつページ書き込み用FIFOへデータを書き込みます。</td>
  </tr>
  <tr>
    <td>Execute</td>
    <td class="tcenter">0xD100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>SPIコマンドシーケンスを実行します。</td>
  </tr>
</tbody>
</table>


### Self Diagnosis System

SDSは自己診断プログラムです。
Xilinx FPGAのIPであるSoft Error Mitigation (SEM)とXADCをそれぞれ実体化し監視しています。
SEMはsingle event upset (SEU)を検知、訂正、分類するIPです。
SDSでは訂正可能なエラーを訂正した回数の取得と、訂正不可能なエラーが発生したことの検知が行えます。
訂正不可能な状態に陥った場合フラッシュメモリからFPGAを再コンフィグするか、パワーサイクルを行う必要があります。
また、意図的にSEUをインジェクトすることが可能ですが、SDSの高度な利用になるのでSEMの利用方法をXilinxのUGで調べて使用してください。

XADCはXilinx FPGAのビルトインADCです。
HULではXADCを利用してFPGAの温度、およびVCCINT、VCCAUX、VCCBRAM電位を取得しています。

<span class="myred">SEMには未解決のトラブルがあります。</span> FPGAの個体によっては電源投入後にuncorrectable errorステータスが1になってしまいます。
原因は分かっていませんが一度SEMをリセットすることで正常に使う事が出来ます。
そのような個体に遭遇したら電源投入後に`hul_software`のreset_semを実行してください。

**RBCP address of SDS (Module ID: 0xC)**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-10">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
        <th>備考</th>
  </tr></thead>
  <tbody>
  <tr>
    <td>SdsStatus</td>
    <td class="tcenter">0xC000'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>SDSモジュールのステータスを取得します。</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>XadcDrpMode</td>
    <td class="tcenter">0xC010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>XADCのDRPモード選択をします。<br>
        <ul>
          <li> 0x0: Read mode</li>
          <li> 0x1: Write mode</li>
        </ul>
    </td>
  </tr>
  <tr>
    <td>XadcDrpAddr</td>
    <td class="tcenter">0xC020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">7</td>
    <td>XADCのDRP addressを与えます。</td>
  </tr>
  <tr>
    <td>XadcDrpDin</td>
    <td class="tcenter">0xC030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">16</td>
    <td>XADCのDRPのデータ入力を与えます。</td>
  </tr>
  <tr>
    <td>XadcDrpDout</td>
    <td class="tcenter">0xC040'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>XADCのDRPのデータ出力を取得します。</td>
  </tr>
  <tr>
    <td>XadcExecute</td>
    <td class="tcenter">0xC0F0'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>XADCのDRPアクセスを実行します。</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>SemCorCount</td>
    <td class="tcenter">0xC100'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>訂正可能なSEUをSEMが訂正した回数を取得します。</td>
  </tr>
  <tr>
    <td>SemRstCorCount</td>
    <td class="tcenter">0xC200'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>SemCorCountをリセットします。</td>
  </tr>
  <tr>
    <td>SemErroAddr</td>
    <td class="tcenter">0xC300'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">40</td>
    <td>SEMのinject_addressポートに入力するアドレスを与えます。</td>
  </tr>
  <tr>
    <td>SemErroStrobe</td>
    <td class="tcenter">0xC400'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>SEMのinject_strobeポートにパルスを入力します。</td>
  </tr>
</tbody>
</table>

**SdsStatusの内容**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">ビット番号</span></th>
    <th class="nowrap"><span class="mgr-10">ステータス名</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">Over temp</td>
    <td>FPGA温度が125℃を超えたことを示します。重大な冷却不足なので直ぐにHULの電源を切り冷やしてください。</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Temp alarm</td>
    <td>FPGA温度が85℃を超えたことを示します。冷却能力が不足していることが考えられます。</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">VCCINT alarm</td>
    <td>VCCINTの電圧が正常範囲 (0.97-1.03V) を超えたことを示しています。何らかのトラブルが基板に起きています。</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">未使用</td>
    <td> </td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">Watchdog alarm</td>
    <td>SEMのheartbeat信号が出ていない事を示します。何らかのトラブルがSEMに起きています。</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Uncorrectable error</td>
    <td>SEMのが訂正不可能な放射線エラーを検出したことを示します。FPGAの再コンフィグが必要です。</td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">未使用</td>
    <td> </td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">未使用</td>
    <td> </td>
  </tr>
  </tbody>
</table>

## HUL Skeleton

このプロジェクトはSiTCPを実装しつつ何もしないfirmwareのほぼ最小構成になります。HUL用のfirmwareを作る際のサンプルとして使ってください。機能は2つしかなく、入力信号をORしてNIMで出力する機能と、SiTCPもしくはDIPでLEDを光らせる機能です。信号のORでは固定入力とメザニン入力（DCRv1かv2を想定）をコネクタ単位でOR（32 OR）して、それぞれ4つのNIMOUTから出力します。

Skeletonの綴りが間違っているのですが直すと影響範囲が広いのでこのままにします。

NIM-Exメザニンカードを利用する際のイグザンプルが`sources_1/example_design/`以下に同梱してあります。
同梱されたtoplevel.vhdを参考にしてください。

**Firmware固有名と現在の最新版**

現在のHUL Skeletonの固有名とバージョンは以下のようになります。
<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x0000</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x02</td>
  </tr>
  </tbody>
</table>

以下バージョンはv3.2のような形式で表記します。

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">-</td>
    <td class="tcenter">-</td>
    <td>v2.xまでの履歴は無し</td>
  </tr>
  <tr>
    <td class="tcenter">v3.2</td>
    <td class="tcenter">2021.08.01</td>
    <td>Version.3系へ変更。</td>
  </tr>
  </tbody>
</table>

### レジスタマップ

**RBCP address of LED (Module ID: 0x0)**

**LED点灯のパターン**<br>
LED1-3はLEDレジスタかもしくはSW2の2-4ビット目で点灯させることが出来ます。
LED4番目はSDSのover temperatureフラグが接続されています。

## HUL RM

HUL RMはmezzanine HRMをマウントすることにより、J0バスマスタとなることができ、なおかつHRMが受信したデータを読み出すDAQモジュールとして動作させることができます。MH-TDCやscalerファームウェアの元となる構成をしており、新しくDAQタイプのファームウェアを開発する際の出発点として利用してください。

Trigger入力に対しる応答はこのファームウェアが基本となっているため、ここではtrigger系統とそれに対するevent builderの応答について詳しく説明します。

**Firmware固有名と現在の最新版**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x0415</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x02</td>
  </tr>
  </tbody>
</table>


**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.0</td>
    <td class="tcenter">2016.12.23</td>
    <td>初期版</td>
  </tr>
  <tr>
    <td class="tcenter">v1.1</td>
    <td class="tcenter">2017.01.15</td>
    <td>RVMのデータヘッダを0x9Cから0xF9へ変更。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.2</td>
    <td class="tcenter">2017.01.27</td>
    <td>Vivado更新 2016.2 => 2016.4 TRM の中間バッファを分散RAMからBRAMへ変更。深さを128から256に変更Prog Full threshold導入。256に設定した理由はSCRブロックの深さを越えないようにするため。それにあわせて、RVMの中間バッファ深さを256へ変更。見かけの機能に変更は無し。 </td>
  </tr>
  <tr>
    <td class="tcenter">v1.3</td>
    <td class="tcenter">2017.03.22</td>
    <td>IOMの初期レジスタが正しく設定できていない問題を解決。電源投入後最初のイベントでヘッダ2に書かれているワード数が0になってしまう問題を解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.4</td>
    <td class="tcenter">2017.05.09</td>
    <td>Clearに応答しない（BUSYが立ちっぱなしになる）問題を解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.5</td>
    <td class="tcenter"> </td>
    <td>HRMを使っていて尚且つClearが入るとハングする問題を解決。（リリースしないままv1.6にとってかわった。）</td>
  </tr>
  <tr>
    <td class="tcenter">v1.6</td>
    <td class="tcenter">2017.08.22</td>
    <td>トリガーが入って2 us程度以内にハードリセットが入るとDAQがハングする問題を修正。ハードリセットへ応答するかどうか、ブロックごとにレジスタで設定できるように変更。新しいローカルアドレス追加。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.7</td>
    <td class="tcenter">2017.12.19</td>
    <td>リセットシーケンスを統一。Header3の24ビット目にHRMが刺さっているかどうか（正確にはDIP2がONかどうか）を示すビットを挿入。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.8</td>
    <td class="tcenter">2018.02.02</td>
    <td>J0バスからやってくるイベントタグをラッチするタイミングが早すぎて、HRM側のイベント番号と1ずれるバグを解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v2.x</td>
    <td class="tcenter"> - </td>
    <td>未公開</td>
  </tr>
  <tr>
    <td class="tcenter">v3.2</td>
    <td class="tcenter">2021.08.01</td>
    <td>FMPとSDSを追加。Builder busの導入。Local busの構造変更。</td>
  </tr>
  </tbody>
</table>

**モジュール動作概要**<br>
HUL RMのデータ収集に関わるブロック図を[以下](#RMFW)に示します。この図ではデータ収集に関わるモジュール以外は省略しています。HUL RMの機能はMezzanine HRMが受信、およびデコードした情報の処理です。そのため、mezzanine slot Uにのみ機能が実装されています。Mezzanine HRMが受信した情報は3つの経路に分配されます。1つはReceiver Module (RVM) であり、lock bit、spill number increment、spill number (full 8bit)、event number (full 12bit) の情報を記憶し、event builder (EVB) に渡します。Event builderへはbuilder busを介してデータが渡されるため、RVMの情報はデータボディに含まれます。すなわち、RVMはEVBに取って計測ブロックの一部です。2つ目の経路はJ0 busへのトリガー情報の配布です。この段階でevent numberは3-bitへspill numberは1-bitへ減らされます。3つ目はTRMへの入力になります。

HULのファームウェアには内部のトリガー配布を管理するtrigger manager (TRM) というモジュールが存在します。TRMは3つのポート、trig Ext (NIM IN)、J0 bus (スレーブの場合)、およびHRM (存在する場合) からトリガー信号を受け取ることができ、どのポートから受け取るかはレジスタで設定します。

TRMはlevel1 trigger, level2 trigger, clear, およびevent number (3-bit)、spill number (1-bit)の情報を各計測ブロックとEVBへ配布します。EVBにとってはこれらはDAQデータではないので、event hearder内にTRMからのタグは埋め込まれます。HULがJ0 busに対してマスターである場合とスレーブである場合とで情報を同等に取り扱わないといけないので、TRMが配布するevent numberとspill numberは3-bitと1-bitに減らされています。Tagを受け取っていない場合event numberもspill numberも0になります。

I/O ManagerはNIM入力ポートをどのFPGA内部信号に割り当てるか、またFPGA内部の信号をNIM出力ポートに割り当てるかを制御するモジュールです。

HULには3種類のbusy信号が存在します。
1つ目はmodule busyであり、これはファームウェア内部のブロックが出力するbusy信号の論理和です。
2つ目はj0 bus busy信号であり、これはJ0 busに対してスレーブなHULのmodule busy信号の論理和です。通常J0 busに対してマスターであるHULが取り扱います。
最後はcrarte busy信号であり、これはmodule busy、J0 bus busy、およびNIM-INポートから入力される外部busy信号の論理和です。これも通常J0 busに対してマスターであるHULが取り扱います。

![rm-fw](rm-fw.png "HUL RMファームウェアのブロック図"){: #RMFW width=90% }

### レジスタマップ

以下はHUL RM専用のマップです。同名のモジュールやレジスタが他のファームウェアに存在しても同一のアドレスであるとは限りません。必ずこのマップ (もしくは配布したソフトウェアのRegisterMap.hh及びnamespace) にしたがって設定してください。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-10">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>TRM内部のトリガーポートの選択を行うレジスタ。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>DAQ gateのON/OFF。DAQ gateが0だとTRMはtriggerを出力できない。</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>このアドレスへ書き込み要求することでEVBのソフトリセットがアサートされ、Event builder内部のセルフイベントカウンタが0になる。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x02)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT1へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT2へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT3へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT4へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL1にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL2にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter">0x2060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext clearにどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter">0x2070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext busy入力にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtRsv2</td>
    <td class="tcenter">0x2080'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext rsv2入力にどのNIMINを接続するか設定。</td>
  </tr>
</table>

**Trigger Manager (TRM)**<br>
TRMはどの入力ポートからの信号をトリガーとして使うのかを決め、FPGA内部用のL1、L2、Clear信号を出力します。また、Tagを受けとっている場合FPGA内部で使うために再配布します。どのポートの信号を選択するかは12-bitのレジスタ、SelectTriggerで設定します。トリガー信号の経路とSelectTriggerの関係を[図](#TRM)にまとめます。L1 triggerをどのポートから受けるかは3つのbitで決まります。2つ以上のbitをセットするとトリガーがORで入ってきてしまいます。L1の経路が決まると次にDAQ gate (DAQ controller管理) とのANDをとり、L1 triggerとして配布されます。L2 triggerとClearの選択も同様に3つのbitで行いますが、これらは経路決定後にEnL2 bitの影響を受けます。EnL2が0の場合L2にはL1のコピーが送信され、Clearは常に0です。MTM-RMシステムを使わない簡易はDAQを行う場合このbitを0にします。その後、L2もDAQ gateとのANDをとり配布されます。Tag情報はJ0から受け取るのかHRMから受け取るのかをEnJ0とEnRM bitsで選択します。両方1にするとTagが出力されません。

![trm-block](trm-block.png "TRM内部のトリガー経路図。"){: #TRM width=90% }

`TRM::SelectTrigger`アドレスに対するレジスタ値のリストです。それぞれのビットが該当するセレクタのスイッチになっているので、このレジスタは整数値ではなく12-bit幅のビット列になります。例外的にRegEnJ0のみTRMの外でも使われます。RegEnJ0がhighであり、かつDIP SW2 2番 (mezzanine HRM) がlowの場合に限り、J0 busへmodule busyを流します。クレートにモジュールを挿入するがJ0 busへは影響を与えたくない場合はこのレジスタをlowにします。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>RegL1Ext</td>
    <td>0x1</td>
    <td>NIMINからL1 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL1J0</td>
    <td>0x2</td>
    <td>J0 busからのL1 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL1RM</td>
    <td>0x4</td>
    <td>Mezzanine HRMからのL1 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL2Ext</td>
    <td>0x8</td>
    <td>NIMINからのL2 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL2J0</td>
    <td>0x10</td>
    <td>J0 busからのL2 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL2RM</td>
    <td>0x20</td>
    <td>Mezzanine HRMからのL2 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegClrExt</td>
    <td>0x40</td>
    <td>NIMINからのClearを選択。</td>
  </tr>
  <tr>
    <td>RegClrJ0</td>
    <td>0x80</td>
    <td>J0 busからのClearを選択。</td>
  </tr>
  <tr>
    <td>RegClrRM</td>
    <td>0x100</td>
    <td>Mezzanine HRMからのClearを選択。</td>
  </tr>
  <tr>
    <td>RegEnL2</td>
    <td>0x200</td>
    <td>0: L2=L1 trigger、1: L2=L2入力</td>
  </tr>
  <tr>
    <td>RegEnJ0</td>
    <td>0x400</td>
    <td>Tag情報にJ0 busの物を採用する。また、このbitが1だとJ0 busへ自身のmodule busyを流す。</td>
  </tr>
  <tr>
    <td>RegEnRM</td>
    <td>0x800</td>
    <td>Tag情報にHRMから受信した値を採用する。</td>
  </tr>
</tbody>
</table>

**I/O Manager (IOM)**<br>
IOMはFPGA内部の信号をNIMOUTへ割り当てたり、NIMINの信号をFPGA内部の信号線へ接続したりする機能を持ちます。たとえば、以下で述べるAddrNnimout1へReg_o_ModuleBusyを設定した場合、フロントパネルのNIM出力1番からmodule busy信号が出力されます。AddrExtL1へReg_i_Nimin1を設定した場合、TRMのL1Ext線にNIM入力1番が割り当てられます。割り当てることの出来るレジスタを以下にまとめます。NIM出力は、出力するNIMポートのアドレスに信号線のレジスタを設定する、NIM入力は内部の信号線のアドレスへNIM入力ポートのレジスタを設定すると、逆の関係になっています。これらのレジスタ値はビット列ではなく整数値として解釈します。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busyです。Module busyは自身の内部busyのみを指します。J0 busのbusyやExtBusyは含まれません。</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusyです。CrateBusyはmodule busyに加えてJ0 busのbusyやExtBusyを含みます。J0 busマスタの場合に利用する信号になり、またHRMが Trigger Moduleへ返すbusyと同等です。</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRMが受信したL1 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>HRMが受信したClearを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>HRMが受信したReserve 1を出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>HRMがSpill Number Incrementを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DAQ gateを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>DIP SW2の8-bit目で設定したレベルを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>内部信号線へ割り当て可能なNIMINポート</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>このレジスタが設定された場合、指定のデフォルト値がそれぞれの内部信号線へ代入されます。</td>
  </tr>
</tbody>
</table>

IOMには初期割り当てが存在します。その初期値を以下に列挙します。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM出力ポート</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">内部信号線</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10">デフォルト値</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
</tbody>
</table>

### HUL上のスイッチ・LEDの機能

**DIP SW2の機能**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">スイッチ番号</span></th>
    <th class="nowrap"><span class="mgr-30">機能</span></th>
    <th class="nowrap"><span class="mgr-10">詳細</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">SiTCP force default</td>
    <td>ONでSiTCPのデフォルトモードで起動します。電源投入前に設定している必要があります。</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Mezzanine HRM</td>
    <td>ONでMezzanine HRMをマウントするためのモードになります。このビットにより切り替わる機能については後述します。このビットがデータのHeader3に現れます。</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">Force BUSY</td>
    <td>Crate BusyとModule Busyを強制的にHighにします。接続チェックなどに使ってください。</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">Bus BUSY</td>
    <td>ONでCrate BusyにJ0 bus busyを含め、OFFで含めません。</td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">LED</td>
    <td>ONでLED4番を光らせます。</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">Level</td>
    <td>IOMのDIP8から出力されるレベルです。</td>
  </tr>
</tbody>
</table>


**Mezzanine HRM (DIP SW2) の機能**<br>
DIP SW2の2-bit目がONになるとMezzanine HRMにかかわる複数の機能が切り替わります。

*Event BuilderがRVMをイベントパケットに含める*<br>
RVMの情報をEvent Builderが読み出してイベントパケットに含めるようになります。

*Mezzanine base Uの入出力方向の変更*<br>
ONでMezzanine HRMにあわせていくつかの信号線がLVDS出力に切り替わります。OFF状態では全ての線はLVDS入力で接続されます。

*J0 busマスタモードをOnにする*<br>
このビットがONだと、J0 busへL1, L2, Clear, Tag情報を流すようになり、なおかつJ0 busに流れるBUSY信号を受け取ることが出来るようになります。ただし、同時にDIP SW1を全てONにしている必要があります。

*J0 busスレーブモードをOFFにする*<br>
このビットがOFFだと、J0 busからL1, L2, Clear, Tagを受け取らなくなります。また、BUSY信号をJ0 busへ流しません。

以下がJ0 bus信号線とトリガー信号の関係です。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">J0 bus信号線</span></th>
    <th class="nowrap"><span class="mgr-20">トリガー信号線</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">S1</td>
    <td>RM_Clear</td>
  </tr>
  <tr>
    <td class="tcenter">S2</td>
    <td>RM_Level2</td>
  </tr>
  <tr>
    <td class="tcenter">S3</td>
    <td>RM_SpillNumber(0)</td>
  </tr>
  <tr>
    <td class="tcenter">S4</td>
    <td>RM_Level1</td>
  </tr>
  <tr>
    <td class="tcenter">S5</td>
    <td>RM_EventNumber(0)</td>
  </tr>
  <tr>
    <td class="tcenter">S6</td>
    <td>RM_EventNumber(1)</td>
  </tr>
  <tr>
    <td class="tcenter">S7</td>
    <td>RM_EventNumber(2)</td>
  </tr>
</tbody>
</table>

**LED点灯の機能**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">LED番号</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">LED1</td>
    <td>点灯中はTCP接続が張られています。</td>
  </tr>
  <tr>
    <td class="tcenter">LED2</td>
    <td>点灯中はmodule busyがhighです。</td>
  </tr>
  <tr>
    <td class="tcenter">LED3</td>
    <td>点灯中はDIP SW2のMezzanine HRMがONであることを意味します。</td>
  </tr>
  <tr>
    <td class="tcenter">LED4</td>
    <td>点灯中はDIP SW2のLEDがONです。</td>
  </tr>
</tbody>
</table>

### DAQの動作

データフローとDAQの動作について述べます。DAQ機能は各計測用モジュール (以下計測ブロック) とevent builderモジュールによって構成されます。データフローを[図](#DAQRM)に示します。トリガーを受信すると各計測ブロックは決められた動作に従いデータを処理してブロックバッファに保存します。計測ブロックは内部にブロックバッファを持ち、マルチイベントを一時的に保存できるようになっています。Event builderは各計測ブロックからデータを読み、イベントバッファがfullでない限りイベントビルドを続けます。そのため、DAQ機能がトリガーに対して同期して動くのはブロックバッファにデータを書き込むまでであり、それから後段の処理は外界の信号や状態に依存せずデータリンクスピードが許す限りビルドと転送を続けます。

HUL RMの場合計測ブロックはRVMとTRMのみなります。TRMの情報はデータボディには含まれずヘッダに含まれるため厳密にはTRMは計測ブロックではありません。イベントビルトされる情報はRVMデータのみであり、TRMの情報はデータの転送制御に使われます。TRMはN番目のイベントにL2 triggerとClearのどちらが送信されたかを保存しており、この情報を使ってevent builderはN番目のイベントパケットをSiTCPに送るか、それともそのまま破棄するかを決定します。そのため、HULのDAQ機能には実質的にはfast clearという動作がなく、clear busyという物も存在しません。L1 riggerによって発生したイベントは必ずビルドされます。ただし、event builderが付与するセルフイベント番号は転送が行われない限りインクリメントされません。

RVMはL2 triggerのタイミングで[図](#DAQRM)に示した情報をラッチしてブロックバッファに保存します。

![daq-rm-fw](daq-rm-fw.png "HUL RM firmwareのDAQデータフロー"){: #DAQRM width=90% }

**Module Busyとなるタイミング**<br>
HUL RMのmodule busyの定義は以下に列挙するbusy信号のORになります。Block fullとSiTCP fullはネットワーク転送が追いつかなくなる限り発生しないため、通常は160 nsの固定長BUSYのみとなります。現在self-busyに160 nsが設定されていますが、若干長めのため将来的に短くする可能性があります。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">BUSY種別</span></th>
    <th class="nowrap"><span class="mgr-30">BUSY長</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">160 ns</td>
    <td>L1 triggerを検出した瞬間から固定長でアサート。</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>ブロックバッファがfullになった段階でbusyが出力されます。L1 triggerレートが後段の回路のデータ処理速度を上回るとアサートされます。つまりTCP転送が追いつかない事を意味するので、実質的にSiTCP fullと同等です。</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>SiTCPのTCPバッファがfullになると出力されます。ネットワークスピードに対してevent builderが送信しようとするデータ量が多いとアサートされます。</td>
  </tr>
</tbody>
</table>

**データ構造**<br>
HULでは32-bitを1ワードとして、3ワードのヘッダと可変長のデータボディを1イベントのブロックとしています。
<br>**Header word**

```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF0415                                         |

Header2 (event size)
|       0xFF       |      0x00       |    "00000"    |         Number of word (11-bit)         |
```
"Number of word" はデータボディに何ワード含まれるかを示します。ヘッダの3ワードは含みません。
```
Header3 (event number)
|       0xFF       | HRM exist | "000" | Tag (4-bit) |         Self counter (16-bit)           |
```
*HRM exist*が1の場合HRMメザニンカードが取り付けられていることを示します。すなわち、データボディにRVMのデータワードが存在します。
*Tag*はTRMから受信した4-bitのタグ情報です。下位3ビットがMTMが送信したイベントナンバーの下位3ビットに相当し、4ビット目がスピルナンバーの最下位ビットです。
*Self counter*はイベントが送信されるたびにインクリメントされるローカルイベント番号です。0から始まります。

**Data body**
```
RVM word
|  0xF9   | "00"  | Lock  | SNI  | Spill Num (8-bit) |            Event Num (12-bit)           |
```

*Lock*はHRMのlock bitです。1でないといけません。*SNI*はspill number incrementであり、1であればスピル番号が変わったことを示しますが、そのように動作しているかどうかはチェックしていません。
*Spill Num*と*Event Num*はそれぞれHRMが受信したスピルナンバーとイベントナンバーです。

## HUL Scaler

HUL ScalerはHUL RMの機能にスケーラを追加したファームウェアです。実装したスケーラは300 MHzでサンプリングを行う、28-bitの同期カウンタです。HUL ScalerはHUL RMと多くの機能が共通のため、異なる点のみ述べます。

**Firmware固有名と現在の最新版**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x4ca1</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  </tbody>
</table>


**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.0</td>
    <td class="tcenter">2016.12.23</td>
    <td>初期版</td>
  </tr>
  <tr>
    <td class="tcenter">v1.1</td>
    <td class="tcenter">2017.01.15</td>
    <td>RVMのデータヘッダを0x9Cから0xF9へ変更。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.2</td>
    <td class="tcenter">2017.01.27</td>
    <td>Vivado更新 2016.2 => 2016.4 TRM の中間バッファを分散RAMからBRAMへ変更。深さを128から256に変更Prog Full threshold導入。256に設定した理由はSCRブロックの深さを越えないようにするため。それにあわせて、RVMの中間バッファ深さを256へ変更。見かけの機能に変更は無し。 </td>
  </tr>
  <tr>
    <td class="tcenter">v1.3</td>
    <td class="tcenter">2017.03.22</td>
    <td>IOMの初期レジスタが正しく設定できていない問題を解決。電源投入後最初のイベントでヘッダ2に書かれているワード数が0になってしまう問題を解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.4</td>
    <td class="tcenter">2017.05.09</td>
    <td>Clearに応答しない（BUSYが立ちっぱなしになる）問題を解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.5</td>
    <td class="tcenter"> </td>
    <td>HRMを使っていて尚且つClearが入るとハングする問題を解決。（リリースしないままv1.6にとってかわった。）</td>
  </tr>
  <tr>
    <td class="tcenter">v1.6</td>
    <td class="tcenter">2017.08.22</td>
    <td>トリガーが入って2 us程度以内にハードリセットが入るとDAQがハングする問題を修正。ハードリセットへ応答するかどうか、ブロックごとにレジスタで設定できるように変更。新しいローカルアドレス追加。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.7</td>
    <td class="tcenter">2017.12.19</td>
    <td>リセットシーケンスを統一。Header3の24ビット目にHRMが刺さっているかどうか（正確にはDIP2がONかどうか）を示すビットを挿入。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.8</td>
    <td class="tcenter">2018.02.02</td>
    <td>J0バスからやってくるイベントタグをラッチするタイミングが早すぎて、HRM側のイベント番号と1ずれるバグを解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v2.x</td>
    <td class="tcenter"> - </td>
    <td>未公開</td>
  </tr>
  <tr>
    <td class="tcenter">v3.3</td>
    <td class="tcenter">2021.08.01</td>
    <td>FMPとSDSを追加。Builder busの導入。Local busの構造変更。</td>
  </tr>
  </tbody>
</table>

**モジュール動作概要**<br>
HUL ScalerではHRMで動作未定義となっていた残りのMezzanine slot Dと固定入力ポートに機能が割り当てられています。Mezzanine slotにはDCR v1 (v2)を実装することを想定しており、最大128ch分のスケーラ値を取得することができます。また、Mezzanine slot UにはDCRの代わりにHRMをマウントすることでHUL RMと同様J0 busマスタになることも可能です。この場合slot Uに割り当てられている32ch分は強制的にデータから削除されます。

Scalerは300 MHz、28-bit長のカウンタで構成されており、L1 triggerのタイミングでカウンタをラッチしてバッファへ書き込みます。HUL scalerでは2つ新しい内部信号がIOMに接続されています。1つはspill gateで、この信号がhighの間のみスケーラはカウントアップします。もう1つはcounter resetで、highになったタイミングでカウンタ値を全てリセットします。NIM入力のカウンターリセットに応答するかどうかは、enable_hdrstで設定可能です (v1.6以降)。他の機能はHUL RMと同様になります。

![scaler-fw](scaler-fw.png "HUL Scaler FWの構造。"){: #SCALERRM width=90% }

### レジスタマップ

以下のテーブルはHUL Scaler専用のレジスタです。同名のモジュールやレジスタが他のfirmwareに存在しても同一のアドレスであるとは限りません。必ずこのマップ (もしくは配布したソフトウェアのRegisterMap.hh及びnamespace) にしたがって設定してください。
HUR RMの異なる部分は<span class="myred">赤字</span>で示してあります。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>TRM内部のトリガーポートの選択を行うレジスタ。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>DAQ gateのON/OFF。DAQ gateが0だとTRMはtriggerを出力できない。</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>このアドレスへ書き込み要求することでEVBのソフトリセットがアサートされ、Event builder内部のセルフイベントカウンタが0になる。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: SCR (module ID = 0x02)</b></td></tr>
   <tr>
    <td><span class="myred">CounterReset</span></td>
    <td class="tcenter"><span class="myred">0x2000'0000</span></td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Software counter resetをアサート。</td>
  </tr>
  <tr>
    <td><span class="myred">EnableBlock</span></td>
    <td class="tcenter"><span class="myred">0x2010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>どのブロック (入力ポート毎) を利用するか設定。ビットをhighにするとそのブロックのデータが取得できるようになります。入力ポートと対応するビットは<br>
      1bit目：Main in U <br>
      2bit目：Main in D <br>
      3bit目：Mezzanine U <br>
      4bit目：Mezzanine D </td>
  </tr>
  <tr>
    <td><span class="myred">EnableHdrst</span></td>
    <td class="tcenter"><span class="myred">0x2020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIM入力のhardware counter resetに応答するかどうかをブロックごとに設定します。ビットが1でそのブロックはhardware counter resetのタイミングでカウンタが0に戻ります。入力ポートと対応するビットは<br>
        1bit目：Main in U<br>
        2bit目：Main in D<br>
        3bit目：Mezzanine U<br>
        3bit目：Mezzanine U </td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x03)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter"><span class="myred">0x3000'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT1へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter"><span class="myred">0x3010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT2へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter"><span class="myred">0x3020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT3へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter"><span class="myred">0x3030'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT4へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter"><span class="myred">0x3040'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL1にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter"><span class="myred">0x3050'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL2にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter"><span class="myred">0x3060'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext clearにどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td><span class="myred">ExtSpillGate</td>
    <td class="tcenter"><span class="myred">0x3070'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>内部信号線のext spill gateにどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td><span class="myred">ExtCCRst</td>
    <td class="tcenter"><span class="myred">0x3080'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>内部信号線のext counter resetにどのNIMINを接続するのか設定。</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter"><span class="myred">0x3090'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext busy入力にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtRsv2</td>
    <td class="tcenter"><span class="myred">0x30A0'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext rsv2入力にどのNIMINを接続するか設定。</td>
  </tr>
  </tbody>
</table>

### 各ブロックの機能

**Trigger Manager (TRM)**<br>
機能およびレジスタはHUL RMと同様です。

**Scaler (SCR)**<br>
スケーラはHUL Scalerの主機能になります。各スケーラユニットは300 MHzで入力信号を同期し、その後エッジ検出を行い、そのエッジのタイミングでカウンタを1つインクリメントします。そのため、検出可能なパルスの最小幅は3.5～4.0 ns程度で、なおかつ2つのパルスを分離するためにも同程度パルスが離れている必要があります。カウンタは28-bit長で一周すると0に戻ります。スケーラは32chごとに4つのブロックに分けられており、EnableBlockレジスタで利用するブロックを設定できます。対応するビットがhighであればそのブロック全体 (32ch分)のデータが返ってきて、lowであれば32ch全て返ってきません。スケーラのリセットはハードリセットのExtCounterReset(NIMIN、IOMで制御)、もしくはソフトリセットのCounterResetアサートによって行うことが出来ます。ハードリセットに応答するかどうかはEnableHdrstでブロックごとに設定できます。このビットが1のブロックはハードリセットのタイミングでカウンタが0になります。また、トリガーから前後100 nsにリセットが入った場合の動作は不定とします。データは返ってきますが、意図しない値が入っているかもしれません。

スケーラのインクリメントはExtSpillGate (NIMIN、IOMで制御) によって制御できます。スケーラはspill gateがhighの時だけインクリメントされます。Spill gateはNIM入力ポートを割り当てると利用することが出来ます。もし未割り当ての場合デフォルトで常にhighになるように設定されています。

**I/O Manager (IOM)**<br>
IOMの機能はHUL RMと基本的に同様ですが、ExtSpillGateとExtCCRstへのNIMINポートの割り当てが追加されています。NIMOUT側は変更ありません。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busyです。Module busyは自身の内部busyのみを指します。J0 busのbusyやExtBusyは含まれません。</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusyです。CrateBusyはmodule busyに加えてJ0 busのbusyやExtBusyを含みます。J0 busマスタの場合に利用する信号になり、またHRMが Trigger Moduleへ返すbusyと同等です。</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRMが受信したL1 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>HRMが受信したClearを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>HRMが受信したReserve 1を出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>HRMがSpill Number Incrementを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DAQ gateを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>DIP SW2 8番のレベルを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>内部信号線へ割り当て可能なNIMINポート</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>このレジスタが設定された場合、指定のデフォルト値がそれぞれの内部信号線へ代入されます。</td>
  </tr>
</tbody>
</table>

以下はIOMレジスタの初期値のテーブルです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM出力ポート</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">内部信号線</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10">デフォルト値</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td><span class="myred">ExtSpillGate</span></td>
    <td>Reg_i_default</td>
    <td>1</td>
  </tr>
  <tr>
    <td><span class="myred">ExtCounterReset</span></td>
    <td>Reg_i_nimin2</td>
    <td>NIMIN2</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
</tbody>
</table>

### HUL上のスイッチ・LEDの機能

**DIP SW2の機能**<br>
HUL RMと同様です。

**LED点灯の機能**<br>
HUL RMと同様です。

### DAQの動作

データフローを[図](#SCALERDAQ)に示します。HUL RMにSCRが追加され、DIP SW2のmezzanine HRMがOFFであればSCRのみからデータを集め、mezzanine HRMがONであればSCRとRVM両方からデータを集めてイベントビルドします。ヘッダ2のnumber of wordにはSCRとRVMの合計ワード数が入ります。

![daq-scaler-fw](daq-scaler-fw.png "HUL Scaler FWのDAQデータパス。"){: #SCALERDAQ width=90% }

**Module Busyとなるタイミング**<br>
HUL Scalerのmodule BUSYの定義は以下に列挙するBUSY信号のORになります。Block fullとSiTCP fullはネットワーク転送が追いつかなくなる限り発生しないため、通常は210 nsの固定長BUSYのみとなります。現在Self-busyに210 nsが設定されていますが、若干長めのため将来的に短くする可能性があります。HUL RMとSelf-busyの長さが異なるのは使っているシステムクロックの周波数が異なるためです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">BUSY種別</span></th>
    <th class="nowrap"><span class="mgr-20">BUSY長</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">210 ns</td>
    <td>L1 triggerを検出した瞬間から固定長でアサート。</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>ブロックバッファがfullになった段階でBUSYが出力されます。L1 triggerレートが後段の回路のデータ処理速度を上回るとアサートされます。つまりTCP転送が追いつかない事を意味するので、実質的にSiTCP fullと同等です。</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>SiTCPのTCPバッファがFullになると出力されます。ネットワーク帯域に対してEvent Builderが送信しようとするデータ量が多いとアサートされます。</td>
  </tr>
</tbody>
</table>

**データ構造**<br>**Header word**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF4CA1                                         |

Header2 (event size)
|       0xFF       |      0x00       |    "00000"    |         Number of word (11-bit)         |
```
*Number of word*はデータボディに何ワード含まれるかを示します。ヘッダの3ワードは含みません。
```
Header3 (event number)
|       0xFF       | HRM exist | "000" | Tag (4-bit) |         Self counter (16-bit)           |
```
*HRM exist*が1の場合HRMメザニンカードが取り付けられていることを示します。すなわち、データボディにRVMのデータワードが存在します。
*Tag*はTRMから受信した4-bitのタグ情報です。下位3ビットがMTMが送信したイベントナンバーの下位3ビットに相当し、4ビット目がスピルナンバーの最下位ビットです。
*Self counter*はイベントが送信されるたびにインクリメントされるローカルイベント番号です。0から始まります。

**Data body**
```
RVM word
|  0xF9   | "00"  | Lock  | SNI  | Spill Num (8-bit) |            Event Num (12-bit)           |
```
*Lock*はHRMのlock bitです。1でないといけません。*SNI*はspill number incrementであり、1であればスピル番号が変わったことを示しますが、そのように動作しているかどうかはチェックしていません。
*Spill Num*と*Event Num*はそれぞれHRMが受信したスピルナンバーとイベントナンバーです。
```
SCR word
|  SCR block (4-bit)   |                            Counter (28-bit)                           |
```
*SCR Block*そのデータがどのSCRブロックに属しているのかを示します。SCRデータにはチャンネル番号を示すフィールドが存在しません。データはチャンネルの番号の低いほうから高いほうへ順番に並んでいます。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">SCR block bits</span></th>
    <th class="nowrap"><span class="mgr-10">入力ポート</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">0x8</td>
    <td class="tcenter">Main in U</td>
  </tr>
  <tr>
    <td class="tcenter">0x9</td>
    <td class="tcenter">Main in D</td>
  </tr>
  <tr>
    <td class="tcenter">0xA</td>
    <td class="tcenter">Mezzanine U</td>
  </tr>
  <tr>
    <td class="tcenter">0xB</td>
    <td class="tcenter">Mezzanine D</td>
  </tr>
  </tbody>
</table>

## HUL MH-TDC

HUL MH-TDCはHUL RMの機能にmulti-hit TDCを追加したファームウェアです。HUL RMと多くの機能が共通のため、異なる点のみ記述します。

**Firmware固有名と現在の最新版**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x30cc</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x04</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.0</td>
    <td class="tcenter">2016.12.23</td>
    <td>初期版</td>
  </tr>
  <tr>
    <td class="tcenter">v1.1</td>
    <td class="tcenter">2017.01.15</td>
    <td>RVMのデータヘッダを0x9Cから0xF9へ変更。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.2</td>
    <td class="tcenter">2017.01.27</td>
    <td>Vivado更新 2016.2 => 2016.4。Block bufferがBuildIn FIFOだったのでBRAMにして深さを4096にした。EventBufferの深さを2048から4096にしてpgfullを4058にした。TDCブロックのchannel bufferを分散RAMからBRAMへ変更。TRMの中間バッファを分散RAMからBRAMへ変更。Prog Full threshold導入。それにあわせて、RVMの中間バッファ深さを128へ変更。
 </td>
  </tr>
  <tr>
    <td class="tcenter">v1.4</td>
    <td class="tcenter">2017.05.09</td>
    <td>Clearに応答しない（BUSYが立ちっぱなしになる）問題を解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.5</td>
    <td class="tcenter">-</td>
    <td>HRMを使っていて尚且つClearが入るとハングする問題を解決。（未リリース）</td>
  </tr>
  <tr>
    <td class="tcenter">v1.6</td>
    <td class="tcenter">-</td>
    <td>一度でもmax multihit (16 hit/ch)を使い切ると、それいこうのイベントがずれる問題に対処。（未リリース）</td>
  </tr>
  <tr>
    <td class="tcenter">v1.7</td>
    <td class="tcenter">2017.08.22</td>
    <td>FPGA内部のイベントシーケンスのバグを修正。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.8</td>
    <td class="tcenter">2017.12.19</td>
    <td>リセットシーケンスを統一。Header3の24ビット目にHRMが刺さっているかどうか（正確にはDIP2がONかどうか）を示すビットを挿入。高負荷な状況で稀にデータが壊れるバグを修正。ヘッダ2のワード数ビット幅を11-bitから12-bitへ変更。</td>
  </tr>
  <tr>
    <td class="tcenter">v1.9</td>
    <td class="tcenter">2018.02.02</td>
    <td>>J0バスからやってくるイベントタグをラッチするタイミングが早すぎて、HRM側のイベント番号と1ずれるバグを解決。
Mezzanine HR-TDCで見られた、サーチウィンドウ外からデータが返ってくるバグの原因をMH-TDCも抱えているため、該当部分を改善した。</td>
  </tr>
  <tr>
    <td class="tcenter">v2.x</td>
    <td class="tcenter"> - </td>
    <td>未公開</td>
  </tr>
  <tr>
    <td class="tcenter">v3.4</td>
    <td class="tcenter">2021.08.01</td>
    <td>FMPとSDSを追加。Builder busの導入。Local busの構造変更。</td>
  </tr>
  </tbody>
</table>

**モジュール動作概要**<br>
HUL MH-TDCではRMで動作未定義となっていた残りのmezzanine slot Dとメイン入力ポートに機能が割り当てられています。Mezzanine slotにはDCR v1 (v2)を実装することを想定しており、最大128ch分のTDCを取得することができます。また、mezzanine slot UにはDCRの代わりにHRMをマウントすることでHUL RMと同様J0 busマスタになることも可能です。この場合slot Uに割り当てられている32ch分のデータは削除されます。

MH-TDCは300 MHz 4相の多相クロック式のTDCを実装しており、1-bitの時間精度は0.83 nsです。Leadingとtrailingの両エッジを検出することができ、トリガーからさかのぼる事の出来る時間の長さは13.7 us、時間分解能は300 ps (σ)です。

![mhtdc-fw](mhtdc-fw.png "HUL MH-TDC FWの構造。"){: #MHTDCFW width=90% }

### レジスタマップ

以下のテーブルはHUL MH-TDC専用のレジスタです。同名のモジュールやレジスタが他のfirmwareに存在しても同一のアドレスであるとは限りません。必ずこのマップ (もしくは配布したソフトウェアのRegisterMap.hh及びnamespace) にしたがって設定してください。
HUR RMの異なる部分は<span class="myred">赤字</span>で示してあります。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>TRM内部のトリガーポートの選択を行うレジスタ。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>DAQ gateのON/OFF。DAQ gateが0だとTRMはtriggerを出力できない。</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>このアドレスへ書き込み要求することでEVBのソフトリセットがアサートされ、Event builder内部のセルフイベントカウンタが0になる。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: TDC (module ID = 0x02)</b></td></tr>
  <tr>
    <td><span class="myred">EnableBlock</span></td>
    <td class="tcenter"><span class="myred">0x2000'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>どのブロック (入力ポート毎) を利用するか設定。ビットをhighにするとそのブロックのデータが取得できるようになります。入力ポートと対応するビットは<br>
      1bit目：Main in U <br>
      2bit目：Main in D <br>
      3bit目：Mezzanine U <br>
      4bit目：Mezzanine D </td>
  </tr>
  <tr>
    <td><span class="myred">PtrOfs</span></td>
    <td class="tcenter"><span class="myred">0x2010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>内部制御変数。ユーザーは触らない。</td>
  </tr>
  <tr>
    <td><span class="myred">WindowMax</span></td>
    <td class="tcenter"><span class="myred">0x2020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の上限値。1-bitが6.666... nsに相当。詳細は後述。</td>
  </tr>
  <tr>
    <td><span class="myred">WindowMin</span></td>
    <td class="tcenter"><span class="myred">0x2030'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の下限値。1-bitが6.666... nsに相当。詳細は後述。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x03)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter"><span class="myred">0x3000'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT1へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter"><span class="myred">0x3010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT2へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter"><span class="myred">0x3020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT3へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter"><span class="myred">0x3030'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT4へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter"><span class="myred">0x3040'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL1にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter"><span class="myred">0x3050'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL2にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter"><span class="myred">0x3060'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext clearにどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter"><span class="myred">0x3070'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext busy入力にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtRsv2</td>
    <td class="tcenter"><span class="myred">0x3080'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext rsv2入力にどのNIMINを接続するか設定。</td>
  </tr>
  </tbody>
</table>

### 各ブロックの機能

**Trigger Manager (TRM)**<br>
機能およびレジスタはHUL RMと同様です。

**Multi-Hit TDC (MH-TDC)**<br>
本ファームウェアの主機能です。本MH-TDCは4相クロックを使うことにより、擬似的に1.2 GHzを作り出しています。[図](#MHTDCDAQ)に示すmulti-hit TDCブロックには、TDC unit、ring buffer、channel bufferの3つのコンポーネントが存在します。TDC unitは擬似1.2 GHzで時間を測定し、ヒット検出を行います。TDC unitの時間分解能は300 ps (σ)、検出可能最小パルス幅はおよそ4 nsです。検出したヒット情報はring bufferに保存されます。ring bufferの長さは13.7 usで、ring bufferの書き込み・読み出しポインタがコースカウントに相当します。Ring bufferは150 MHzのクロックで駆動されているため、コースカウントは6.666...ns精度です。

L1 triggerを検出するとring bufferからの読み出しを開始します。この時、ヒットを探す範囲を`WindowMax`レジスタおよび`WindowMin`レジスタによって設定可能です。これらのレジスタは1-bitがコースカウント精度の11-bitの整数値です。この範囲に入らないヒットはchannel bufferへ書き込まれません。Ring bufferからヒット情報を探している間busyが出力されます。

Channel bufferからblock bufferへデータをまとめる際に、1 ch/evenに許される最大ヒット数が設定されます。1 ch/eventに許されるヒットはTDC chの大きいほうから16ヒットまでです。それ以上のヒットがchannel bufferに記録されていた場合、16を超えるデータは破棄され、overflowビットが立ちます。

<table class="vmgr-table">
  <tr><td class="tcenter" colspan=2><b>Multi-hit TDC仕様</b></td></tr>
  <tr>
    <td>TDC精度</td>
    <td class="tcenter">0.833... ns</td>
  </tr>
  <tr>
    <td>コースカウント精度</td>
    <td class="tcenter">6.66... ns</td>
  </tr>
  <tr>
    <td>Ring buffer長</td>
    <td class="tcenter">13.8 us</td>
  </tr>
  <tr>
    <td>時間分解能</td>
    <td class="tcenter">300 ps (σ) *実測</td>
  </tr>
  <tr>
    <td>最小パルス幅</td>
    <td class="tcenter">~4 ns</td>
  </tr>
  <tr>
    <td>ダブルヒット分解能</td>
    <td class="tcenter">~7 ns</td>
  </tr>
  <tr>
    <td>最大ヒット数/ch/event</td>
    <td class="tcenter">16</td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**

<table class="vmgr-table">
  <thead><tr>  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busyです。Module busyは自身の内部busyのみを指します。J0 busのbusyやExtBusyは含まれません。</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusyです。CrateBusyはmodule busyに加えてJ0 busのbusyやExtBusyを含みます。J0 busマスタの場合に利用する信号になり、またHRMが Trigger Moduleへ返すbusyと同等です。</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRMが受信したL1 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>HRMが受信したReserve 1を出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>HRMがSpill Number Incrementを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DCTのDAQ gateを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>DIP SW2 8番のレベルを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>内部信号線へ割り当て可能なNIMINポート</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>このレジスタが設定された場合、指定のデフォルト値がそれぞれの内部信号線へ代入されます。</td>
  </tr>
</tbody>
</table>

以下はIOMレジスタの初期値のテーブルです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM出力ポート</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">内部信号線</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10">デフォルト値</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
</tbody>
</table>

### HUL上のスイッチ・LEDの機能

**DIP SW2の機能**<br>
HUL RMと同様です。

**LED点灯の機能**<br>
HUL RMと同様です。

### DAQの動作

データフローを[図](#MTDCDAQ)に示します。HUL RMにMH-TDCが追加され、DIP SW2のmezzanine HRMがOFFであればTDCのみからデータを集め、mezzanine HRMがONであればTDCとRVM両方からデータを集めてイベントビルトします。ヘッダ2のNumber Of WordにはTDCとRVMの合計ワード数が入ります。

![daq-mhtdc-fw](daq-mhtdc-fw.png "HUL MH-TDC FWのDAQデータフロー。"){: #MTDCDAQ width=90% }

**Module Busyとなるタイミング**<br>
HUL MH-TDCのmodule busyの定義は以下に列挙するbusy信号のORになります。普段はsequence busy分のbusy長となるはずです。現在self-busyに210 nsが設定されていますが、若干長めのため将来的に短くする可能性があります。HUL RMとself-busyの長さが異なるのは使っているシステムクロックの周波数が異なるためです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">BUSY種別</span></th>
    <th class="nowrap"><span class="mgr-20">BUSY長</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">210 ns</td>
    <td>L1 triggerを検出した瞬間から固定長でアサート。</td>
  </tr>
  <tr>
    <td>Sequence busy</td>
    <td class="tcenter">サーチ窓幅に依存</td>
    <td>Ring bufferからヒット情報を探している間、すなわち`(WindowMax-WindowMin) x 6.66.. ns`分のbusyが出力されます。</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>ブロックバッファがfullになった段階でBUSYが出力されます。L1 triggerレートが後段の回路のデータ処理速度を上回るとアサートされます。つまりTCP転送が追いつかない事を意味するので、実質的にSiTCP fullと同等です。</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>SiTCPのTCPバッファがFullになると出力されます。ネットワーク帯域に対してEvent Builderが送信しようとするデータ量が多いとアサートされます。</td>
  </tr>
</tbody>
</table>

**データ構造**<br>**Header word**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF30CC                                         |

Header2 (event size)
|       0xFF       |      0x00       |    "0000"    |         Number of word (12-bit)          |
```
*Number of word*はデータボディに何ワード含まれるかを示します。ヘッダの3ワードは含みません。
```
Header3 (event number)
|       0xFF       | HRM exist | "000" | Tag (4-bit) |         Self counter (16-bit)           |
```
*HRM exist*が1の場合HRMメザニンカードが取り付けられていることを示します。すなわち、データボディにRVMのデータワードが存在します。
*Tag*はTRMから受信した4-bitのタグ情報です。下位3ビットがMTMが送信したイベントナンバーの下位3ビットに相当し、4ビット目がスピルナンバーの最下位ビットです。
*Self counter*はイベントが送信されるたびにインクリメントされるローカルイベント番号です。0から始まります。

**Data body**
```
RVM word
|  0xF9   | "00"  | Lock  | SNI  | Spill Num (8-bit) |            Event Num (12-bit)           |
```
*Lock*はHRMのlock bitです。1でないといけません。*SNI*はspill number incrementであり、1であればスピル番号が変わったことを示しますが、そのように動作しているかどうかはチェックしていません。
*Spill Num*と*Event Num*はそれぞれHRMが受信したスピルナンバーとイベントナンバーです。
```
TDC word
|  Magic word (8-bit)  |  "0" + Ch (7-bit)  | "00" |            TDC (14-bit)                   |
```
*Magic word*の定義は以下のようになります。
<ul>
  <li> 0xCC Leading
  <li> 0xCD Trailing
</ul>

*Ch*には0から127までのチャンネル番号が入ります。チャンネルアサインについては2章を参照してください。
*TDC*はTDCデータです。

## Mezzanine HR-TDC 及びHUL HR-TDC BASE

Mezzanine HR-TDC内部のファームウェアとそれを制御するためのファームウェアの説明です。Mezzanine HR-TDCはHUL MH-TDCでいうblock bufferまでの機能を別FPGAへ実装したファームウェアになり、HUL HR-TDC BASEはそれ以降のevent builderやtrigger managerといったDAQ全体を管理する機能を実装したファームウェアになります。よって、Mezzanine HR-TDCは複雑な動作はできません。Trigger（Common stop）に応答して測定データをHULへ転送するだけがその機能になります。一方で、その制御系はFPGAが2つあるため若干複雑です。また、Mezzanine HR-TDCはtapped-delay-lineのcalibration LUTを持っていたり、データ転送のためにDDR通信を行っていたりと他のファームウェアにない特徴を持っています。ここではこれらの機能と制御方法について説明します。

**Mezzanine HR-TDC固有名と現在の最新版**<br>
以前はリーディングエッジのみ測定できるファームウェアと両エッジ測定できるファームウェアは区別されていましたが、新しいファームウェアでは1つに統合されました。測定するエッジの選択はレジスタで行います。

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x80cc</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x05</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x00</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v2.5</td>
    <td class="tcenter">2017.12.19</td>
    <td>リーディング測定FWの初期版</td>
  </tr>
  <tr>
    <td class="tcenter">v2.6</td>
    <td class="tcenter">2018.02.02</td>
    <td>サーチウィンドウ外からデータが返ってくるバグを解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v3.2</td>
    <td class="tcenter">2017.12.19</td>
    <td>リーディング・トレーリング測定FWの初期版</td>
  </tr>
  <tr>
    <td class="tcenter">v3.3</td>
    <td class="tcenter">2018.02.02</td>
    <td>サーチウィンドウ外からデータが返ってくるバグを解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v4.5</td>
    <td class="tcenter">2021.08.01</td>
    <td>SEMとXADCの導入。Builder busの導入。測定エッジのレジスタによる選択を可能に変更。<br>
    XDC上にあった軽微な間違いを修正 (v4.3とは機能的に変わらない)。<br>
    このバージョンまでTDCサンプリングクロックとシステムクロックの周波数は520 MHzと130 MHz。
    </td>
  <tr>
    <td class="tcenter">v5.0</td>
    <td class="tcenter">2023.01.17</td>
    <td>メザニンカードからのトリガー信号出力を実装した。<br>
    Local bus bridgeを修正した。Mezzanine FW v5.0はv3.7以前のHUL HRTDC BASEファームウェアと互換性がない。<br>
    TDCサンプリングクロックとシステムクロックを500 MHzと125 MHzへ変更。
    </td>
  </tr>
  </tbody>
</table>

**モジュール動作概要**<br>
Mezzanine HR-TDCとHUL HR-TDC BASEを接続した状態のブロック図を[図](#BLOCKHRTDC)に示します。制御系が他のファームウェアよりも複雑なため、多少詳しく描いています。HR-TDCのシステムはそれぞれのFPGAにBCTが存在し、mezzanine HR-TDC側はBASE側のBusBridgeを通じて2段階アクセスでレジスタを制御します。BsuBridgeを通じたMezzanine側の制御については専用のC++関数が用意されており、ソフトウェアのセクションでもう一度説明します。

時間測定のみを行うMezzanine HR-TDCと、イベントビルトやトリガー制御、および各IOの管理を行うBASE側に分かれます。トリガー情報の管理は他のモジュール同様にTrigger Manager (TRM)が行います。Mezzanine HR-TDCへはTRMが出力したlevel 1 triggerのみが送信されます。この信号がmezzanine HR-TDCにとってはcommon stop信号です。TDCとしての動作はHUL MH-TDCと同等です。時間精度だけがよりよくなったと思ってください。ヒットを記録するためのリングバッファ長は15.7 us、時間分解能は25 ps (σ) (common stopに対して)、20 ps (σ) (チャンネル間の差分)です。

DAQデータをmezzanineからBASEへ高速で転送するために、5線の信号線を使ってデータを送っています。制御ビットを含んで転送しているため5線の帯域を全て使えているわけではなく、1ワード (32 bit) 転送するのにかかる時間は8 ns (4 Gbps) です。BASE側のDDR receiverは電源投入後に一度初期化する必要があります。この方法についてもソフトウェアの節で説明します。TDC baseは送られてきたデータを見て、event builderに渡す準備をします。

![blocks-hrtdc-fw](blocks-hrtdc-fw.png "HUL HR-TDC FWのブロック構成。"){: #BLOCKHRTDC width=90% }

### Mezzanine HR-TDCについての詳細説明

**高分解能時間測定の原理**<br>
本ファームウェアはtapped-delay-line（TDL）方式という時間測定方法をとっています。TDLの概念を[図](#TDL)に示します。TDLはごく細かい遅延量を持つ素子を直列につないだ遅延ライン上を、入力信号がどこまで走ったかをフリップフロップで記録することにより、クロックエッジ間の時間情報の補間を行う技術です。[図](#TDL)の灰色の四角形は遅延素子を示し、各遅延素子の間の情報をD-FFアレイが毎クロックスナップショットを取っています。遅延素子を以後tapと呼ぶことにします。スナップショットを取るためのクロックは500 MHz (2.0 ns)のため、2.0 nsの間にパルスが到達可能な最大のtap番号が分かればtap当たりの遅延量dTが分かります。これは十分な数の統計があれば、tap番号のエンドポイントがそれに相当します。簡単には「2000 ps/最大tap番号」がtapあたりのdTであり、TDC 1-bit分の時間になります。ところが、FPGA HR-TDCではtapの遅延量はすべて異なるので、静的な校正ではなく、全てのtap番号を時間に変換する校正が必要になります。ここで、tap番号をfine count、fine countから時間に変換された値をestimatorと呼びます。

![hrtdc-tdl](hrtdc-tdl.png "Tapped-delay-lineの概念図。"){: #TDL width=90% }

**時間校正**<br>
Estimatorを生成するための手順を[図](#ESTIMATOR)に示します。横軸をtap番号として、時間相関のない入力対するヒストグラムを生成します。各ビンの重さがdTの値に比例します。ここでヒストグラムの総エントリー数は固定値であり、ある値 (0x7ffff)に達するまでヒストグラムへのデータフィルを続けます。ヒストグラムが用意出来たら各ビンカウントを積分しestimatorを生成します。i番目のビンのエントリー数をwiとすると、N番目のestimatorは
```math
E_{n} = w_{n}/2 + \sum^{n-1}_{0}(w_{i})
```
として計算されます。そのため、FPGA内部にはfine count ヒストグラムを生成するための機構とfine countからestimatorへ変換して出力する回路が必要になります。最も分かりやすい方法は検出器の信号を使ってfine count ヒストグラムを生成する方法です。（検出信号はランダムである必要があります。）ヒストグラム生成はDAQとは無関係であるため、入力信号は全て自動的にヒストグラムへフィルされていきます。しかし、この方法では0x7ffffまでイベント溜まるまで待たないといけなく、また、検出器が繋がっているチャンネルでしか利用できません。そのため、クロックを使ってヒストグラムを生成する方法を用意しています。FPGA内部で校正専用のクロックを全入力ラインに接続します。このクロックはTDLをサンプリングするクロックに対して毎エッジ少しずつ位相がずれていくように調整されています。原理的には1psずつ異なった場所に校正クロックのエッジを立てることが出来ます。校正クロックを使った方法では数十msでヒストグラムを生成することが出来ます。電源投入後のモジュールの初期化やRUNの最初に校正することを想定しています。

![hrtdc-estimator](hrtdc-estimator.png "Fine count histogramからEsitomatorを生成する手順。"){: #ESTIMATOR width=90% }

**実装されている校正ブロックと動作**<br>
時間校正のシステムは2つのRAMによって実装されます。[図](#HRTDCLUT)に示すようにfine countは2つのRAMへ同時に入力されます。片方のRAMはfine countからestimatorへ変換する仕事をします。生のestimatorは19-bit幅でそのままでは過剰なので、下位8-bitを捨てて11-bit幅にしてから出力します。対してもう一方のRAMではヒストグラムの生成を行っています。0x7ffffイベント溜まった時点でestimatorへ変換し、RAMのスワップ待ちの状態になります。RAMスワップは片方が準備できたら自動的に切り替えるか、手動で気切り替えるかを選ぶことが出来ます。これをつかさどっているのが、`Controll::AutoSw`と`ReqSwitch`です。`Controll::AutoSw`が1であれば自動切り替え、0であれば`ReqSwitch`へ書き込み動作をするとRAMが手動で切り替わります。自動切り替えはRUN中であっても検出器の信号を使ってRAMを常に更新したい場合に利用します。

また、estimatorへ変換せずにfine countをそのまま取り出したい場合もあると思います。その場合は、`Controll::Through`を1にすることでfine countが直接現れます。

![hrtdc-lut](hrtdc-lut.png "Estimator LUTの切り替えパターン。"){: #HRTDCLUT width=90% }

**HR-TDC system**<br>
Mezzanine HR-TDC内部のHR-TDC機能についてまとめます。本ファームウェアではTDLの長さは192 tapsですが、そのままだと遅延時間が細かすぎるため、3つのtapを1つにまとめて64の有効tapに変換しています。そのため、fine countの最大値は63となります。測定した結果では、2.0 nsでパルスが走る距離はおおよそ55 tapsです。出力されたfine countは125 MHzのクロック領域に渡され、estimatorへ変換されたのち、ring bufferへhit bitと一緒に書き込まれます。[図](#HRTDCBLOCK)にあるようにestimator (11bit) + semi-coarse count (2bit) + coarse count (11bit)で24bitのデータ長になります。その後、mezzanine HR-TDCの中で一度部分的にイベントビルドされ、HUL側へ転送されます。簡単に時間に直すにはestimatorの最大値2048と500 MHzのクロックの値を使って、
時間 = TDC value/2048/ClkFreq (ClkFreq = 0.50 GHz (FW ver 5.0 以降) もしくは = 0.52 GHz (FW ver 4.5以前))
とすることで時間 (ns)に直ります。もっと正確に時間に直したい場合はTDC calibratorを使ってください。
Ring buffer以降の実装はMH-TDCと同一です。L1 trigger（common stop）を検出するとring bufferからの読み出しを開始します。この時、ヒットを探す範囲を`WindowMax`レジスタおよび`WindowMin`レジスタによって設定可能です。これらのレジスタは1-bitがコースカウント精度の11-bitの整数値です。この範囲に入らないヒットはchannel bufferへ書き込まれません。Ring bufferからヒット情報を探している間busyが出力されます。MH-TDCとは使っているシステムクロックが異なるため、coarse count精度が異なります。

その後channel bufferからblock bufferへデータをまとめる際に、1ch/eventに許される最大ヒット数が設定されます。1ch/eventに許されるヒットはTDC chの大きいほうから16ヒットまでで、それ以上のヒットがchannel bufferに記録されていた場合、16を超えるデータは破棄され、overflowビットが立ちます。

**トリガー出力**<br>
Version 5.0よりHR-TDCからトリガー信号を出力できるようになりました。
前述のhit bitを全てのチャンネルに対して論理和取り出力します。
トリガー出力を利用しない場合、TrigMaskレジスタによりチャンネル毎にマスクする事が出来ます。

<table class="vmgr-table">
  <tr><td class="tcenter" colspan=2><b>High-resolution TDC 仕様</b></td></tr>
  <tr>
    <td>TDC精度</td>
    <td class="tcenter">~30 ps</td>
  </tr>
  <tr>
    <td>コースカウント精度</td>
    <td class="tcenter">8.0 ns</td>
  </tr>
  <tr>
    <td>Ring buffer長</td>
    <td class="tcenter">16.3 us</td>
  </tr>
  <tr>
    <td>時間分解能</td>
    <td class="tcenter">20 ps (σ) *実測</td>
  </tr>
  <tr>
    <td>最小パルス幅</td>
    <td class="tcenter">~2 ns</td>
  </tr>
  <tr>
    <td>ダブルヒット分解能</td>
    <td class="tcenter">~4 ns</td>
  </tr>
  <tr>
    <td>最大ヒット数/ch/event</td>
    <td class="tcenter">16</td>
  </tr>
  </tbody>
</table>

![hrtdc-tdcblock](hrtdc-tdcblock.png "HR-TDCのブロック構造。"){: #HRTDCBLOCK width=90% }

### レジスタマップ

Mezzanine HR-TDCのレジスタについてまとめます。ここに記述しているレジスタは`RegisterMap.hh`内で`namespace HRTDC_MZN`に属するものです。
Mezzanine HR-TDCへはBus Bridge Primary (BBP)を通じてバスブリッジしてアクセスするため、RBCPのアドレスで直接指定することは出来ません。
**Version 5.0よりlocal bus bridgeを修正したことでアドレス範囲が12-bit幅から16-bit幅へ拡張されました。**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: DCT (module ID = 0x0)</b></td></tr>
  <tr>
    <td>TestMode</td>
    <td class="tcenter">0x0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>HUL側のDDR receiverを初期化するためにDDR transmitterからテストパターンを出力するモード。電源投入時のモジュール初期化に必要で、配布しているC++ソフトウェアのddr_initialize内部で使用する。</td>
  </tr>
  <tr>
    <td>ExtraPath</td>
    <td class="tcenter">0x0010</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>このビットを立てると信号入力経路が基板上の入力ポートから校正クロックに切り替わる。前述のestimatorを校正クロックによって生成するために使用。</td>
  </tr>
  <tr>
    <td>Gate</td>
    <td class="tcenter">0x0020</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>DAQ gate。1だとcommon stopがHR-TDCへ入力される。</td>
  </tr>
  <tr>
    <td>EnBlocks</td>
    <td class="tcenter">0x0030</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">2</td>
    <td>Leading/trailing測定ブロックを有効化する。1ビット目がleading block、2ビット目がtrailing block。デフォルトで0のため必ず設定する必要がある。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: TDC (module ID = 0x01)</b></td></tr>
  <tr>
    <td>Control</td>
    <td class="tcenter">0x1010</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>HR-TDCの動作を変えるためのレジスタ。以下の3つのビットが存在。<br>
        Through 	(0x1)<br>
        AutoSw 	  (0x2)<br>
        StopDout 	(0x4)<br>
        Throughが1だとfine countはestimatorへ変換されずにそのまま出力される。AutoSwが1だと前述のestimator RAMが準備出来次第RAMのスワップを行う。StopDoutが1だとFPGA内部でcommon stopとの引き算を取らずに、stopのデータも1ワードとして転送する。</td>
  </tr>
  <tr>
    <td>ReqSwitch</td>
    <td class="tcenter">0x1020</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>`AutoSw`が0の場合、このレジスタへ書き込みを行おうとするとestimator RAMのスワップを行う。</td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0x1030</td>
    <td class="tcenter">R</td>
    <td class="tcenter">1</td>
    <td>1であれば次のEstimator RAMの準備が出来ていることを表す。</td>
  </tr>
  <tr>
    <td>PtrOfs</td>
    <td class="tcenter">0x1040</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>内部制御変数。ユーザーは触らない。</td>
  </tr>
  <tr>
    <td>WindowMax</td>
    <td class="tcenter">0x1050</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の上限値。1bitが8.0 nsに相当。詳細はMH-TDCを参照。</td>
  </tr>
  <tr>
    <td>WindowMin</td>
    <td class="tcenter">0x1060</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の下限値。1bitが8.0 nsに相当。詳細はMH-TDCを参照。</td>
  </tr>
  <tr>
    <td>TrigMask</td>
    <td class="tcenter">0x1070</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">32</td>
    <td>各チャンネルのトリガー出力をマスクする。各ビットがチャンネルに対応しており、0にセットするとマスクされる。例：チャンネル0をマスクする場合、0xFFFF'FFFEをセットする。</td>
  </tr>

  <tr><td class="tcenter" colspan=5><b>DAQ Controller: SDS (module ID = 0xC)</b></td></tr>
  <tr>
    <td>SdsStatus</td>
    <td class="tcenter">0xC000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>SDSモジュールのステータスを取得します。</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>XadcDrpMode</td>
    <td class="tcenter">0xC010</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>XADCのDRPモード選択をします。<br>
        <ul>
          <li> 0x0: Read mode</li>
          <li> 0x1: Write mode</li>
        </ul>
    </td>
  </tr>
  <tr>
    <td>XadcDrpAddr</td>
    <td class="tcenter">0xC020</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">7</td>
    <td>XADCのDRP addressを与えます。</td>
  </tr>
  <tr>
    <td>XadcDrpDin</td>
    <td class="tcenter">0xC030</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">16</td>
    <td>XADCのDRPのデータ入力を与えます。</td>
  </tr>
  <tr>
    <td>XadcDrpDout</td>
    <td class="tcenter">0xC040</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>XADCのDRPのデータ出力を取得します。</td>
  </tr>
  <tr>
    <td>XadcExecute</td>
    <td class="tcenter">0xC050</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>XADCのDRPアクセスを実行します。</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>SemCorCount</td>
    <td class="tcenter">0xC0A0</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>訂正可能なSEUをSEMが訂正した回数を取得します。</td>
  </tr>
  <tr>
    <td>SemRstCorCount</td>
    <td class="tcenter">0xC0B0</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>SemCorCountをリセットします。</td>
  </tr>
  <tr>
    <td>SemErroAddr</td>
    <td class="tcenter">0xC0C0</td>
    <td class="tcenter">W</td>
    <td class="tcenter">40</td>
    <td>SEMのinject_addressポートに入力するアドレスを与えます。</td>
  </tr>
  <tr>
    <td>SemErroStrobe</td>
    <td class="tcenter">0xC0D0</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>SEMのinject_strobeポートにパルスを入力します。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: BCT (module ID = 0xE)</b></td></tr>
  <tr>
    <td>Reset</td>
    <td class="tcenter">0xE000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Bus Controllerからモジュールリセット信号をアサートし、SiTCPを除く全モジュールを初期化。</td>
  </tr>
  <tr>
    <td>Version</td>
    <td class="tcenter">0xE010</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Firmwareの固有名とバージョンを読み出す。多バイト読み出しが必要。</td>
  </tr>
  <tr>
    <td>Reconfig</td>
    <td class="tcenter">0xE020</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>PROG_B_ONをLowにしてFPGAの再コンフィギュレーションを行う。一度通信が切れるので暫くしてから再接続。</td>
  </tr>
  </tbody>
</table>

### Mezzanine上のスイッチ・LEDの機能

**DIP SWの機能**<br>
基板上の4bit DIPスイッチに割り当てられている機能です。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">スイッチ番号</span></th>
    <th class="nowrap"><span class="mgr-30">機能</span></th>
    <th class="nowrap"><span class="mgr-10">詳細</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">-</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">-</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">-</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">-</td>
    <td>未使用</td>
  </tr>
</tbody>
</table>

**LED点灯の機能**<br>
Mezzanine HR-TDCにはユーザーが利用できるLEDはありません。赤いLEDはFPGAがコンフィグされていることを示すLEDです。

**Module Busyとなるタイミング**<br>
Mezzanine HR-TDCのbusyの定義は以下に列挙するbusy信号のORになります。実際には更にHUL HR-TDC BASEのbusyもORしたものがシステムのbusyとなります。普段はsequence busy分のbusy長となるはずです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">BUSY種別</span></th>
    <th class="nowrap"><span class="mgr-20">BUSY長</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Sequence busy</td>
    <td class="tcenter">サーチ窓幅に依存</td>
    <td>Ring bufferからヒット情報を探している間、すなわち`(WindowMax-WindowMin) x 8.0 ns`分のBUSYが出力されます。</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>ブロックバッファがfullになった段階でBUSYが出力されます。L1 triggerレートが後段の回路のデータ処理速度を上回るとアサートされます。つまりTCP転送が追いつかない事を意味するので、実質的にSiTCP fullと同等です。</td>
  </tr>
</tbody>
</table>

### HUL HR-TDC BASEについての詳細説明

HUL HR-TDC BASEはDDR receiverとBctBusBridgeを除けばその機能は殆どMH-TDCと同じです。ですが、MH-TDCと違いHRMを実装することはできません。そのため、J0 bus masterになることはできません。

DDR receiverは電源投入後一度初期化する必要がある以外はユーザーが能動的にアクセスすることはありません。BctBusBridgeはmezzanine slot upとdownにそれぞれ独立で用意されています。BusBridgeはHUL側のBCTとメザニン側のBCTの橋渡しをします。HUL側のBCTからはBusBridgeはローカルモジュールの一種、メザニン側のBCTからは外部リンクのように見えます。メザニンへアクセスするためには2度のアクションを行う必要があります。1回目のアクションでHUL側のBCTはBusBridgePrimary (BBP)に対して、読み書きのコマンド値、メザニン側のlocal address、および書き込むべきレジスタ値（書き込みを行う予定の場合）をBBP内部へ格納します。2回目のアクションで`BBP::Exec`を呼び出すと、バスブリッジを実行し通信を行います。メザニン側のBCTを書き込みで駆動するか、読み出しで駆動するかは、1回目のアクションで指定したコマンド値で決まります。BBPは通信プロセスが正しく終わるまでHUL側のBCTへ応答しません。<span class="myred">そのため、例えばmezzanine HR-TDCが刺さっていないのに`BBP::Exec`を呼ぶとHUL側のBCTがデッドロックします。</span>この場合`BCT::Reset`を呼ぶことが出来なくなるのでSiTCP Resetで対応する必要があります。BusBridge制御のためのC++関数は`BctBusBridgeFunc.cc`にまとめられています。詳細は6章で述べます。

**HR-TDC BASE固有名と現在の最新版**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x80eb</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x04</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x01</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.5</td>
    <td class="tcenter">2017.12.19</td>
    <td>初期バージョン</td>
  </tr>
  <tr>
    <td class="tcenter">v1.6</td>
    <td class="tcenter">-</td>
    <td>未公開</td>
  </tr>
  <tr>
    <td class="tcenter">v1.7</td>
    <td class="tcenter">2018.02.02</td>
    <td>J0バスからやってくるイベントタグをラッチするタイミングが早すぎて、HRM側のイベント番号と1ずれるバグを解決。
BCT::Resetを呼ぶとBCTがハングするバグを解決。</td>
  </tr>
  <tr>
    <td class="tcenter">v3.7</td>
    <td class="tcenter">2021.08.01</td>
    <td>SDS, FMPの追加。Builder busの導入。BCTの構造変更。</td>
  </tr>
  <tr>
    <td class="tcenter">v4.0</td>
    <td class="tcenter">2023.01.17</td>
    <td>Mezzanine HR-TDC v5.0.対応版。Mezzanine HR-TDC v4.5以前のバージョンはサポートしていない。</td>
  </tr>
   <tr>
    <td class="tcenter">v4.1</td>
    <td class="tcenter">2023.02.24</td>
    <td>v4.0が正しく動作しないことがあるバグを発見。v4.1はその対策版。</td>
  </tr>
  </tbody>
</table>

### レジスタマップ

これはHUL HR-TDC BASE側のレジスタマップであり、RegisterMap.hh内部でnamespace HRTDC_BASEに属します。Mezzanine側と名前が被るものもあるので、必ずnamespaceで指定してください。IOMにおいてHRMをサポートしなくなったことによっていくつかのレジスタが存在しません。

加えて本ファームウェア用の`RegisterMap.hh`には先頭に`kEnSlotup`と`kEnSlotDown`というグローバル変数が記述されています。これはどのスロットにmezzanine HR-TDCが刺さっているかを示すフラグです。Mezzanine HR-TDCが刺さっていないスロットはfalseにしてください。各実験の本番用ソフトに移植する際には無くても良い変数です。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>TRM内部のトリガーポートの選択を行うレジスタ。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>DAQ gateのON/OFF。DAQ gateが0だとTRMはtriggerを出力できない。</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>このアドレスへ書き込み要求することでEVBのソフトリセットがアサートされ、Event builder内部のセルフイベントカウンタが0になる。</td>
  </tr>
  <tr>
    <td>InitDDR</td>
    <td class="tcenter">0x1020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>DDR receiverへ向けて初期化要求を行う。</td>
  </tr>
  <tr>
    <td>CtrlReg</td>
    <td class="tcenter">0x1030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>DDR receiverを制御するためのレジスタ。詳細は後述。</td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0x1040'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">4</td>
    <td>DDR receiverのステータスレジスタ。詳細は後述。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x02)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT1へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT2へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT3へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>NIMOUT4へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL1にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL2にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter">0x2060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext clearにどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter">0x2070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext busy入力にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td class="tcenter">0x2090'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Mezzanine HR-TDC内部のcoarse countをリセットするハードリセット信号。複数台のHR-TDCを同期したい場合に使用する。この線にどのNIMINを接続するか設定。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Bus Bridge Primary: BBP (module ID = 0x3, 0x4)</b></td></tr>
  <tr>
    <td>Txd</td>
    <td class="tcenter">0x3000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">32</td>
    <td>Slot-UのセカンダリFPGA (mezzanine card上のFPGA) に対してlocal bus bridgeを介してへ書き込むデータ。</td>
  </tr>
  <tr>
    <td>Rxd</td>
    <td class="tcenter">0x3010'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Slot-UのセカンダリFPGAからlocal bus bridgeを介して読み出したデータ。
    </td>
  </tr>
  <tr>
    <td>Exec</td>
    <td class="tcenter">0x3100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Bus bridge primaryを駆動しslot-UのセカンダリFPGAと通信を行うためのスタート信号をアサートする。</td>
  </tr>
  <tr>
  <td>Txd</td>
    <td class="tcenter">0x4000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">32</td>
    <td>Slot-DのセカンダリFPGA (mezzanine card上のFPGA) に対してlocal bus bridgeを介してへ書き込むデータ。</td>
  </tr>
  <tr>
    <td>Rxd</td>
    <td class="tcenter">0x4010'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Slot-DのセカンダリFPGAからlocal bus bridgeを介して読み出したデータ。</td>
  </tr>
  <tr>
    <td>Exec</td>
    <td class="tcenter">0x4100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Bus bridge primaryを駆動しslot-DのセカンダリFPGAと通信を行うためのスタート信号をアサートする。</td>
  </tr>
  </tbody>
</table>

**Trigger Manager (TRM)**<br>
HUL HR-TDC BASEはHRMをマウントすることが出来ないため、TRMのRMに関するレジスタは機能しません。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>RegL1Ext</td>
    <td>0x1</td>
    <td>NIMINからL1 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL1J0</td>
    <td>0x2</td>
    <td>J0 busからのL1 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL2Ext</td>
    <td>0x8</td>
    <td>NIMINからのL2 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegL2J0</td>
    <td>0x10</td>
    <td>J0 busからのL2 triggerを選択。</td>
  </tr>
  <tr>
    <td>RegClrExt</td>
    <td>0x40</td>
    <td>NIMINからのClearを選択。</td>
  </tr>
  <tr>
    <td>RegClrJ0</td>
    <td>0x80</td>
    <td>J0 busからのClearを選択。</td>
  </tr>
  <tr>
    <td>RegEnL2</td>
    <td>0x200</td>
    <td>0: L2=L1 trigger、1: L2=L2入力</td>
  </tr>
  <tr>
    <td>RegEnJ0</td>
    <td>0x400</td>
    <td>Tag情報にJ0 busの物を採用する。また、このbitが1だとJ0 busへ自身のmodule busyを流す。</td>
  </tr>
</tbody>
</table>

**DAQ controller (DCT)**

DCT内部の`CtrlReg`と`Status`が示すビットの詳細を述べます。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-30">Bit番号</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>CTRLレジスタの内訳</b></td></tr>
  <tr>
    <td>RegTestModeU</td>
    <td>1st bit (0x1)</td>
    <td>このビットを立てると、DDR receiver (slot-U)を初期化するために、テストパターンを受信するモードへ切り替える。</td>
  </tr>
  <tr>
    <td>RegTestModeD</td>
    <td>2nd bit (0x2)</td>
    <td>このビットを立てると、DDR receiver (slot-D)を初期化するために、テストパターンを受信するモードへ切り替える。</td>
  </tr>
  <tr>
    <td>EnableU</td>
    <td>3rd bit (0x4)</td>
    <td>このビットを立てるとDDR receiver (slot-U)が利用可能になる。</td>
  </tr>
  <tr>
    <td>EnableD</td>
    <td>4th bit (0x8)</td>
    <td>このビットを立てるとDDR receiver (slot-D)が利用可能になる。</td>
  </tr>
  <tr>
    <td>FRstU</td>
    <td>5th bit (0x10)</td>
    <td>このビットを立てるとSlot-UのFPGAに倒してフォースリセット信号をアサートする。v3.7以前のファームウェアではMIFブロックに実装されていた機能。</td>
  </tr>
  <tr>
    <td>FRstD</td>
    <td>6th bit (0x20)</td>
    <td>このビットを立てるとSlot-DのFPGAに倒してフォースリセット信号をアサートする。v3.7以前のファームウェアではMIFブロックに実装されていた機能。</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>Statusレジスタの内訳</b></td></tr>
  <tr>
    <td>BitAlignedU</td>
    <td>1st bit (0x1)</td>
    <td>DDR receiver (slot-U)のbit slipが終了し、データ読み出しが可能になった事を示す。</td>
  </tr>
  <tr>
    <td>BitAlignedD</td>
    <td>2nd bit (0x2)</td>
    <td>DDR receiver (slot-D)のbit slipが終了し、データ読み出しが可能になった事を示す。</td>
  </tr>
  <tr>
    <td>BitErrorU</td>
    <td>3rd bit (0x4)</td>
    <td>DDR receiver (slot-U)のbit slipを一定回数施行したが、正しい結果が返ってこなかった事を示す。初期化失敗。</td>
  </tr>
  <tr>
    <td>BitErrorD</td>
    <td>4th bit (0x8)</td>
    <td>DDR receiver (slot-D)のbit slipを一定回数施行したが、正しい結果が返ってこなかった事を示す。初期化失敗。</td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**

HRMをサポートしないため複数のレジスタが機能しません。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busyです。Module busyは自身の内部busyのみを指します。J0 busのbusyやExtBusyは含まれません。</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DCTのDAQ gateを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>DIP SW2 8番のレベルを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_TrigOutU</td>
    <td class="tcenter">0xD</td>
    <td>Slot-Uのmezzanine HR-TDCから出力されるトリガー出力をアサインします。</td>
  </tr>
  <tr>
    <td>Reg_o_TrigOutD</td>
    <td class="tcenter">0xE</td>
    <td>Slot-Dのmezzanine HR-TDCから出力されるトリガー出力をアサインします。</td>
  </tr>
  <tr>
    <td>Reg_o_TrigOutUD</td>
    <td class="tcenter">0xF</td>
    <td>Slot-U/D両方のmezzanine HR-TDCから出力されるトリガー出力の論理和をアサインします。</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>内部信号線へ割り当て可能なNIMINポート</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>このレジスタが設定された場合、指定のデフォルト値がそれぞれの内部信号線へ代入されます。</td>
  </tr>
</tbody>
</table>

IOMの初期レジスタです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM出力ポート</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">内部信号線</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10">デフォルト値</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
</tbody>
</table>

**DIP SW2の機能**<br>
DIP SW2に割り当てられている機能を列挙します。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">スイッチ番号</span></th>
    <th class="nowrap"><span class="mgr-30">機能</span></th>
    <th class="nowrap"><span class="mgr-10">詳細</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">SiTCP force default</td>
    <td>ONでSiTCPのデフォルトモードで起動します。電源投入前に設定している必要があります。</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Not in use</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">Force BUSY</td>
    <td>Crate busyとmodule busyを強制的にhighにします。接続チェックなどに使ってください。</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">Bus BUSY</td>
    <td>ONでCrate BusyにJ0 bus busyを含め、OFFで含めません。</td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">LED</td>
    <td>ONでLED4番を光らせます。</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">Level</td>
    <td>IOMのDIP8から出力されるレベルです。</td>
  </tr>
</tbody>
</table>

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">LED番号</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">LED1</td>
    <td>点灯中はTCP接続が張られています。</td>
  </tr>
  <tr>
    <td class="tcenter">LED2</td>
    <td>点灯中はmodule busyがhighです。</td>
  </tr>
  <tr>
    <td class="tcenter">LED3</td>
    <td>DAQ gateがONであると点灯。</td>
  </tr>
  <tr>
    <td class="tcenter">LED4</td>
    <td>点灯中はDIP SW2のLEDがONです。</td>
  </tr>
</tbody>
</table>

### DAQの動作

データフローを[図](#DAQHRTDC)に示します。Mezzanine HR-TDCとBASEが2つのFPGAへ分かれていますが、見かけの動作はHUL MH-TDCと変わりません。各mezzanineではblock bufferに相当する部分までの部分的なイベントビルドが行われデータがBASEへ転送されてきます。BASE側では受け取ったデータをまとめてイベントビルドします。

![daq-hrtdc-fw](daq-hrtdc-fw.png "Mezzanine HR-TDCとHUL HR-TDC BASEを合わせたDAQのデータフローブロック図"){: #DAQHRTDC width=90% }

**Module Busyとなるタイミング**<br>
システム全体のBUSY条件をのべます。Module busyはこれらのORです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">BUSY種別</span></th>
    <th class="nowrap"><span class="mgr-30">BUSY長</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">210 ns</td>
    <td>L1 triggerを検出した瞬間から固定長でアサート。</td>
  </tr>
  <tr>
    <td>Mezzanine busy</td>
    <td class="tcenter">Mezzanineの状態に依存</td>
    <td>Mezzanine HR-TDCが出力しているBUSY。通常はサーチ窓幅のbusyが返ってきます。</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>TDC BaseのブロックバッファがFullになった段階でbusyが出力されます。つまりTCP転送が追いつかない事を意味するので、実質的にSiTCP fullと同等です。</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>SiTCPのTCPバッファがFullになると出力されます。ネットワーク帯域に対してEvent Builderが送信しようとするデータ量が多いとアサートされます。</td>
  </tr>
</tbody>
</table>

**データ構造**<br>**ヘッダワード**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF80EB                                         |

Header2 (event size)
|   0xFF00   |   OvrFlow   |    "000"    |            Number of word (12-bit)                  |
```
*Number of word*はデータボディに含まれるワード数を示します。*Number of Word*はSub-header分の2ワード分を含みます。なので最低値が2です。*OverFlow*はHUL全体で1chでもover flowチャンネルがあると立ちます。
```
Header3 (event number)
|   0xFF   | "0000" |  Tag (4-bit)  |                 Self counter (16-bit)                    |
```
*Tag*はTRMから出力される4bitのTag情報です。下位3bitがRM event Numberの下位3ビット、4ビット目がRM spill numberの最下位ビットとなります。*Self counter*はイベント転送を行うたびにインクリメントされるlocal event numberで、0オリジンです。

```
Sub-headers
|   0xFA00  | "0" |  OverFlow  |  StopDuout  |  Through  |          # of word (12-bit)         |
|   0xFB00  | "0" |  OverFlow  |  StopDuout  |  Through  |          # of word (12-bit)         |
```
それぞれmezzanine HR-TDCのヘッダです。*0xFA00*と*0xFB00*がそれぞれslot-Uと-Dに対応します。*OverFlow*はそれぞれのメザニン中でのover flowの存在を示しています。*Stop dout*と*Through*はそれぞれ`HRTDC_MZN::TDC::Controll`における`StopDout`と`Through`の状態を示します。*Number of word*は各メザニンのワード数を示します。

**Data body**
```
|  Magic word (3-bit)  |  Ch (5-bit)  |                     TDC (24-bit)                       |
```
*Magic word*は以下のように定められています。
<ul>
  <li>6	Leading
  <li>5	Trailing
  <li>4	Common stop
</ul>
となります。Chは5 bitしかない事が示すように31chまでしか数えることが出来ません。サブヘッダA・Bに属するデータであるかを見てデコードして、サブヘッダBに属するのであれば32ch足してください。TDCはHR-TDCの節で述べたようにestimator (11bit) + semi-coarse count (2bit) + coarse count (11 bit)で合計24 bitです。ThroughがONの場合fine countはestimatorの位置に現れます。

## Three-dimensional matrix trigger

このファームウェアのバージョン表記は他のFWと異なっており、メジャーバージョンが1ですがFMPやSDSは搭載されています。

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0xe033</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x01</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x01</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.1</td>
    <td class="tcenter">2020.12.02</td>
    <td>実験で利用した最終版</td>
  </tr>
  </tbody>
</table>

**動作概要**<br>
このファームウェアはJ-PARC E03実験とE42実験の際に用いられたマトリックスコインシデンストリガーです。似たような機能を持つマトリックストリガー回路を作成したい場合の例題として利用してください。本ファームウェアにはデータ収集機能はありません。このFWでは3種類のホドスコープの三次元相関からトリガーを生成します。[図](#MTXFW)にブロック図を示します。この実験ではBH2、TOF、SCHという3種類のタイミングホドスコープ間のマトリックス相関を用います。丸カッコ内の数字はチャンネル番号を表しており、合計14336パターン (8x28x64) の組み合わせが発生します。入力は二重FFで同期されます。クロック速度は350 MHzです。後述のDWGやマトリックスパターンのブロックも同様のクロックで駆動されています。BH2がNIM-INへも繋がれているのはテストを簡便に行うためです。このファームウェアはメザニンスロットがSCHの入力を受けるためDCR v1/v2が必須です。

同期された入力信号はDelay Width Generator (DWG) で幅と遅延時間の調整がされます。各DWGはRBCPを通じて調整が可能です。350 MHz (2.857... ns) のクロック精度で32段階の調整が可能です。詳しくはソフトウェアの項で述べます。DWGではパルス出力中にもう一度パルス入力があった場合2つのパルスを繋げます。麻痺型モデルで表されるデッドタイムの振る舞いと同様です。

MTX3DとMTX2Dはそれぞれ三次元マトリックスコインシデンスと二次元マトリックスコインシデンスのブロックです。このFWでは1つ三次元トリガーと2つの二次元トリガーが実装されており、それぞれマトリックスパターンを設定可能です。RBCPを用いて全てのマトリックスエレメント1つ1つのOn/Offの切り替えが可能です。どのように実現しているかは後述します。

各マトリックストリガーはIOMを通じてNIMOUTから出力可能です。このFWでは実験の要求から二次元マトリックスの出力を三次元マトリックスの結果でVETOする経路が用意されています。この実験では三次元トリガーがビームVETOの役割を果たしていたためタイミングが良く分かっており、二次元トリガーに対しては固定長ディレイでVETO位置を調整しています。このあたりは実験条件に合わせて変更してください。


![mtx3d-fw](mtx3d-fw.png "Matrix3D triggerのブロック図。左側入力、右側が出力。"){: #MTXFW width=90% }

### マトリックスパターンの設定方法

14336パターンのスイッチをアドレスで解決しようと思うととてつもなく大きなエンコーダが必要になり、一般的にFPGAで実現するのは非現実的です。ここで3次元のレジスタを(28x64)x8と分解すると、8-bit幅・1792長のレジスタと捉えることが出来ます。すなわち、8-bit幅で長さが1792のシフトレジスタを用意することで、全てのレジスタビットの設定が可能となります。二次元の場合1-bit幅で長さが1792のシフトレジスタを用意します。RBCP (BCT) は同一のアドレスに1792回書き込むだけで設定が実現でき、極めてリソース効率が良いです。レジスタ設定はシステムクロックに対して低速なクロックで行っても良いため、このFWでは10 MHzのクロックでシフトレジスタを駆動しています。

### DWGの構造

DWGは遅延と幅の生成をシフトレジスタへのビットパターンのプリセットによって実現しています。例えば先頭から`00011111000...`というビットパターンを入力があったタイミングでシフトレジスタへセットしたとします。そうするとこのパターンは遅延量が3でパルス幅が5の波形を与えます。遅延量と幅の情報からビットパターンへの変換はソフトで行う事にしています。最大幅と遅延量がそれぞれ32のため、DWGの取るレジスタ幅は64-bitです。

### レジスタマップ

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: MTX2D-1 (module ID = 0x00)</b></td></tr>
  <tr>
    <td>TofDWG</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (1-24) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TofExtDWG</td>
    <td class="tcenter">0x0010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (25-28) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>SchDWG</td>
    <td class="tcenter">0x0020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SCH入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TrigDWG</td>
    <td class="tcenter">0x0030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>二次元トリガー出力のDWGを設定します。</td>
  </tr>
  <tr>
    <td>EnableMtx</td>
    <td class="tcenter">0x0040'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">1</td>
    <td>二次元マトリックスパターンを設定するアドレスです。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>ExecSR</td>
    <td class="tcenter">0x0100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>二次元マトリックスパターンを設定するシフトレジスタを1つシフトさせます。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: MTX2D-2 (module ID = 0x01)</b></td></tr>
  <tr><td class="tcenter" colspan=5>各レジスタはMTX2D-1と同様です</td></tr>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: MTX3D (module ID = 0x02)</b></td></tr>
  <tr>
    <td>TofDWG</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (1-24) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TofExtDWG</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (25-28) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>SchDWG</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SCH入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>Bh2DWG</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SCH入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TrigDWG</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>三次元トリガー出力のDWGを設定します。</td>
  </tr>
  <tr>
    <td>EnableMtx</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">8</td>
    <td>三次元マトリックスパターンを設定するアドレスです。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>ExecSR</td>
    <td class="tcenter">0x2100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>三次元マトリックスパターンを設定するシフトレジスタを1つシフトさせます。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: IOM (module ID = 0x03)</b></td></tr>
  <tr>
    <td>Nimout1</td>
    <td class="tcenter">0x3000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout1ポートへの出力を設定します。</td>
  </tr>
  <tr>
    <td>Nimout2</td>
    <td class="tcenter">0x3010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout2ポートへの出力を設定します。</td>
  </tr>
  <tr>
    <td>Nimout3</td>
    <td class="tcenter">0x3020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout3ポートへの出力を設定します。</td>
  </tr>
  <tr>
    <td>Nimout4</td>
    <td class="tcenter">0x3030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout4ポートへの出力を設定します。</td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**

<table class="vmgr-table">
  <thead><tr>  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_Trig3D</td>
    <td class="tcenter">0x0</td>
    <td>MTX3Dトリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2DVeto1</td>
    <td class="tcenter">0x1</td>
    <td>MTX3DでVETOされた後のMTX2D-1トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2DVeto2</td>
    <td class="tcenter">0x2</td>
    <td>MTX3DでVETOされた後のMTX2D-2トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2D1</td>
    <td class="tcenter">0x3</td>
    <td>MTX2D-1トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2D2</td>
    <td class="tcenter">0x4</td>
    <td>MTX2D-2トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_TofOR</td>
    <td class="tcenter">0x5</td>
    <td>TofORを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_SchOR</td>
    <td class="tcenter">0x6</td>
    <td>SchORを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Bh2OR</td>
    <td class="tcenter">0x7</td>
    <td>Bh2ORを出力します。</td>
  </tr>
  </tbody>
</table>

## Mass trigger (TOF based trigger)

**このファームウェアは2023年現在更新がストップしています。Mezzanine HR-TDC v5.0や最新のソフトウェアとは互換性がありません。以前のバージョンの物を引き続き利用してください。**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x20d1</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x00</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v3.0</td>
    <td class="tcenter">2020.12.02</td>
    <td>実験で利用した最終版</td>
  </tr>
  </tbody>
</table>

Mass trigger (MsT) はJ-PARC K1.8での通称であり、機能的にはTOFベースのトリガー生成回路です。ダイポール磁気スペクトロメータを通過した粒子の軌道は大雑把に粒子の運動量と相関があります。2つのホドスコープの二次元マトリックス相関から運動量範囲を制限し、各マトリックスエレメントに対してTOFの分布を取ると粒子の質量ごとにTOFピークが分離します（低い運動量であれば）。Mass triggerはmezzanine HR-TDCとHULのメイン入力を用いて、二次元マトリックスとTOF情報によるトリガー生成を行うためのファームウェアです。HR-TDCの情報を得るためにはcommon stop入力が必要であるため、mass triggerはlevel2 decisionを行い、level2 triggerかもしくはfast clear信号を生成することが仕事です。

ブロック図を[図](#MSTFW)に示します。MsTはHRMとHR-TDCのメザニンカードを必要としています。それぞれ、slot-Uとslot-Dへマウントしてください。Main INへ入力された信号は低速なTDCでデジタイズされヒットレジスタ情報を与えます。そのため、このファームウェアにおけるTOFは厳密には検出器間の時間差でなく、common stopとの時間差です。Mass triggerを利用するためにはlevel1 triggerが非同期回路で生成されている必要があります。この仕様はK1.8ビームラインの都合に合わせてあるため、真のTOFを計算させたい場合ファームウェアの改修が必要になります。

SCH (64ch)とTOF (32ch) 間の2048パターンに対してTOF windowのチェックを行います。この時、HR-TDCから返ってきたマルチヒットデータ全てに対して判定を行うため、同一チャンネルに対して複数回の判定が行われることがあります。1つでもアクセプト範囲にTOF値があればトリガーが発行されます。1つのアクセプト範囲にデータがない場合クリア信号が出力されます。アクセプト窓の設定はSiTCP経由で行います。MsTでトリガー判定を行いたいのはlevel1 triggerが物理トリガーの場合だけです。キャリブレーショントリガーの場合は判定無しでlevel2 triggerを発生させなければいけません。判定すべきlevel1 triggerかどうかを知らせるために、このファームウェアではpiK flagという信号をlevel1 triggerに続いて入力することになっています。Flag入力が無ければ判定を行わず、必ずlevel2 triggerが発行されます。

本ファームウェアにはデータ収集機能が存在します。HR-TDCとLR-TDCによって得られた時間情報と、MsTの判定結果がデータとして返ってきます。本ファームウェアはトリガー生成回路ですが、データ収集のためには外部からトリガー入力が必要です。

![mst-fw](mst-fw.png "Mass triggerのブロック図。"){: #MSTFW width=90% }

**タイミングチャート**<br>
K1.8ビームラインでmass triggerを導入する目的は、主にVMEモジュール用にfast clearを生成し不必要なバスアクセス時間を減らす事です。K1.8ビームラインではchained-block-transfer (CBLT) でVMEバスアクセスを行っていますが、おおよそ100 usのバスアクセス時間がかかります。VMEモジュール内のmulti-event bufferを活用して、バスアクセス時間をlevel1 triggerに対するbusyに含めない工夫はしていますが、潜在的なbusyであるためlevel1 triggerのレートが7 kHzあたりから急激にDAQ効率が悪くなります。そのような実験ではmass triggerを導入してlevel2判定を加えてバスアクセスの回数を減らします。

[図](#MSTCHART)に想定タイミングチャートを示します。Mass triggerはHR-TDCとLR-TDCにcommon stop入力が入った時点から動作を開始します。HR-TDCからデータの転送が終わると判定を開始します。Mass triggerはmulit-hitを全て処理するため、ここまでの時間は可変です。Level1から固定時間後に判定結果を出力するために`MST::TimerPreset`で出力までの時間を設定します。今のファームウェアバージョンでは500程度の大きな値を設定することを推奨しています。`MST::TimerPreset`は判定プロセスよりも優先度が高いため、指定時間後に判定が終わっていなかった場合後述のno decision flagを立てたうえで強制的にlevel2 triggerを出力します。No decision flagが頻繁に立つ場合`MST::TimerPreset`の値が小さすぎます。

![mst-timing](mst-timing.png "想定しているlevel1 triggerからlevel2判定までのタイムチャート。"){: #MSTTIMING width=90% }

**判定フロー**<br>
Level2 trigger判定のフロー図を[図](#MSTCHART)に示します。図の下側に書かれているリストは各判定状態におけるフラグ（および内部信号）の状況を示しています。判定動作はlevel1 trigger受信で開始します。TDCブロックから情報を集め判定回路が各チャンネル独立に動作します。`MST::TimerPreset`に指定した時間がたった後、piK flagを受信しているか、および判定プロセスがすべて終了しているかのチェックを行います。どちらかを満たしていない場合、no decision flagを立ててlevel2 triggerを出力します。次にHR-TDCから取得したTDC値がアクセプト範囲にあるかどうかのチェックを行います。1つでも存在すればmass trigger acceptととなり、level2 triggerが出力されます。1つもない場合clear判定となり、敗者復活判定へ続きます。Clear判定を下されたイベントはDAQで取得されないため、クリアしているイベントをサンプル検査するために`MST::ClearPreset`に指定した値に1度、敗者復活アクセプトを出します。敗者復活判定が下された場合consolation acceptのフラグが立ち、level2 triggerが出力されます。敗者復活の条件を満たさない場合fast clearが出力されます。

![mst-chart](mst-chart.png "Mass triggerの判定フロー。"){: #MSTCHART width=90% }

**TOFのアクセプト範囲の設定**<br>
Mass triggerには2種類のTDC範囲設定を行うレジスタが存在します。`LRTDC::WinMax`と`LRTDC::WinMin`、およびmezzanine HR-TDCのウィンドウレジスタはcommon stop入力に対してヒットサーチを行う範囲を設定します。これは通常のTDCファームウェアと同様です。`MST::WixMax`と`MST::WinMin`はlevel2判定でアクセプトとなる範囲を与えます。Mass triggerでは32 chのHR-TDC入力と64 chのLR-TDC入力間の二次元マトリックスに対して、行列要素毎にこのレンジを設定することが出来ます。マトリックスコインシデンストリガーの時と同様に24-bit幅で長さが2048のシフトレジスタを採用しています。`MST::WixMax`と`MST::WinMin`の両方を0に設定すると、その行列要素に対しては判定を行いません。

### レジスタマップ

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>TRM内部のトリガーポートの選択を行うレジスタ。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>DAQ gateのON/OFF。DAQ gateが0だとTRMはtriggerを出力できない。</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>このアドレスへ書き込み要求することでEVBのソフトリセットがアサートされ、Event builder内部のセルフイベントカウンタが0になる。</td>
  </tr>
  <tr>
    <td>InitDDR</td>
    <td class="tcenter">0x1020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>DDR receiverへ向けて初期化要求を行う。</td>
  </tr>
  <tr>
    <td>CtrlReg</td>
    <td class="tcenter">0x1030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>DDR receiverを制御するためのレジスタ。</td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0x1040'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">4</td>
    <td>DDR receiverのステータスレジスタ。</td>
  </tr>
    <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x02)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT1へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT2へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT3へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT4へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL1にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL2にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter">0x2060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext clearにどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter">0x2070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext busy入力にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td class="tcenter">0x2090'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Mezzanine HR-TDC内部のcoarse countをリセットするハードリセット信号。複数台のHR-TDCを同期したい場合に使用する。この線にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>PiKTrig</td>
    <td class="tcenter">0x20A0'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Mass triggerに対する物理トリガーフラグ入力。Level1 triggerに続いて入力があるとlevel2判定を行う。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: MIF-Down</b></td></tr>
  <tr>
    <td>Connect</td>
    <td class="tcenter">0x3000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">-</td>
    <td>MIF-Downからmezzanine HR-TDCのBCTへ向けて通信プロセスを開始する。アクセスする際のモードが書き込みなのか読み出しなのかで、メザニンのBCTへのアクセス方法が切り替わる仕様となっている。</td>
  </tr>
  <tr>
    <td>Reg</td>
    <td class="tcenter">0x3010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">20</td>
    <td>MIF-Downにmezzanine HR-TDC用のlocal addressと書き込み用レジスタ値をMIFに一時的に保存する。<br>
        <ul>
          <li> [19:8]: Local address</li>
          <li> [7:0]: Register valus</li>
        </ul>
    </td>
  </tr>
  <tr>
    <td>ForceReset</td>
    <td class="tcenter">0x3100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>MIF-Downのmezzanine HR-TDCへ強制リセット信号をアサート。DAQやBCTがハングした場合に使用する。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Low-resolution TDC: LRTDC</b></td></tr>
  <tr>
    <td>PtrOfs</td>
    <td class="tcenter">0x4010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>内部制御変数。ユーザーは触らない。</td>
  </tr>
  <tr>
    <td>WindowMax</td>
    <td class="tcenter">0x4020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の上限値。1-bitが6.666... nsに相当。</td>
  </tr>
  <tr>
    <td>WindowMin</td>
    <td class="tcenter">0x4030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の下限値。1-bitが6.666... nsに相当。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Mass trigger: MST</b></td></tr>
  <tr>
    <td>ClearPreset</td>
    <td class="tcenter">0x5000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">7</td>
    <td>このレジスタに書かれた値に1度、敗者復活アクセプトが出力される。0に設定すると敗者復活判定を行わない。</td>
  </tr>
  <tr>
    <td>TimerPreset</td>
    <td class="tcenter">0x5010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">9</td>
    <td>Level1 triggerを受信してからlevel2 trigger/clearを出力するまでの遅延時間。</td>
  </tr>
  <tr>
    <td>WinMax</td>
    <td class="tcenter">0x5020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">24</td>
    <td>アクセプトするTOF範囲の上限値を与えます。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>WinMin</td>
    <td class="tcenter">0x5030'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">24</td>
    <td>アクセプトするTOF範囲の下限値を与えます。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>Exec</td>
    <td class="tcenter">0x5040'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>シフトレジスタを1つシフトさせます。</td>
  </tr>
  <tr>
    <td>Bypass</td>
    <td class="tcenter">0x5050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>このレジスタが1になると判定回路をバイパスしてlevel2 triggerを出力するようになります。実験中に一時的にmass triggerをバイパスしたい場合に利用します。</td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**

<table class="vmgr-table">
  <thead><tr>  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busyです。Module busyは自身の内部busyのみを指します。J0 busのbusyやExtBusyは含まれません。</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusyです。CrateBusyはmodule busyに加えてJ0 busのbusyやExtBusyを含みます。J0 busマスタの場合に利用する信号になり、またHRMが Trigger Moduleへ返すbusyと同等です。</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRMが受信したL1 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>HRMが受信したReserve 1を出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>HRMがSpill Number Incrementを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DCTのDAQ gateを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>DIP SW2 8番のレベルを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Accept</td>
    <td class="tcenter">0xD</td>
    <td>Accept flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Clear</td>
    <td class="tcenter">0xE</td>
    <td>Clear flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_ConsolationAccept</td>
    <td class="tcenter">0xF</td>
    <td>Consolation accept flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_FinalClear</td>
    <td class="tcenter">0x10</td>
    <td>Final clear flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Level2</td>
    <td class="tcenter">0x11</td>
    <td>Level2 flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_FastClear</td>
    <td class="tcenter">0x12</td>
    <td>Fast clear flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_NoDecision</td>
    <td class="tcenter">0x13</td>
    <td>No decision flagを出力します。</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>内部信号線へ割り当て可能なNIMINポート</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>このレジスタが設定された場合、指定のデフォルト値がそれぞれの内部信号線へ代入されます。</td>
  </tr>
</tbody>
</table>

以下はIOMレジスタの初期値のテーブルです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM出力ポート</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_RML1</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_RML2</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_RMClr</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">内部信号線</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10">デフォルト値</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>PiKFlag</td>
    <td>Reg_i_nimin2</td>
    <td>NIMIN2</td>
  </tr>
</tbody>
</table>

### HUL上のスイッチ・LEDの機能

**DIP SW2の機能**<br>
HUL RMと同様です。

**LED点灯の機能**<br>
HUL RMと同様です。

### DAQの動作

Mass triggerからはLR-TDCとHR-TDCの測定結果、およびlevel2判定の結果（フラグ）が返ってきます。Level2判定はイベント毎に行うため、level2判定を行っている間BUSYになります。

**Module Busyとなるタイミング**<br>
BUSYの定義は以下に列挙するBUSY信号のORになります。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">BUSY種別</span></th>
    <th class="nowrap"><span class="mgr-20">BUSY長</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">210 ns</td>
    <td>L1 triggerを検出した瞬間から固定長でアサート。</td>
  </tr>
  <tr>
    <td>Sequence busy</td>
    <td class="tcenter">サーチ窓幅に依存</td>
    <td>Ring bufferからヒット情報を探している間、すなわち`WindowMax`-`WindowMin`分のBUSYが出力されます。Mass triggerにはLR-TDCとHR-TDCが存在するため、長いほうが採用されます。</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>ブロックバッファがfullになった段階でBUSYが出力されます。L1 triggerレートが後段の回路のデータ処理速度を上回るとアサートされます。つまりTCP転送が追いつかない事を意味するので、実質的にSiTCP fullと同等です。</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>SiTCPのTCPバッファがFullになると出力されます。ネットワーク帯域に対してEvent Builderが送信しようとするデータ量が多いとアサートされます。</td>
  </tr>
  <tr>
    <td>Decision busy</td>
    <td class="tcenter"> `MST::TimerPreset`に依存</td>
    <td>Level1 trigger受信からlevel2/clear出力までの間BUSYになります。`MST::TimerPreset`に依存します。</td>
  </tr>
</tbody>
</table>

**データ構造**<br>**ヘッダワード**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF20D1                                         |

Header2 (event size)
|       0xFF00       |  OerFlow  |    "000"    |         Number of word (12-bit)               |
```
Number of wordはデータボディに含まれるワード数を示します。Number of WordはSub-header分の2ワード分を含みます。なので最低値が2です。Over flowはHUL全体で1chでもover flowチャンネルがあると立ちます。
```
Header3 (event number)
|   0xFF   | HRM bit | "000" |  Tag (4-bit)  |             Self counter (16-bit)               |
```
TagはTRMから出力される4bitのTag情報です。下位3bitがRM Event Numberの下位3ビット、4ビット目がRM spill numberの最下位ビットとなります。Self-counterはイベント転送を行うたびにインクリメントされるlocal event numberで、0オリジンです。

```
Data body
RVM data
|  0xF9  | "00" |  Lock  |  SNI  |  Spill Num (8-bit))  |           Event Num (12-bit)         |

Mass trigger data
|                 0x2000                    |   "0000'0000'0"   |   Mass trigger flag (7-bit)  |

HR-TDC block
Sub-hearder
|  0x8000  | "00" |  OerFlow  |  StopDuout  |  Through  |           # of word (11-bit)         |
HR-TDC data
|  Magic word (3-bit)  |  Ch (5-bit)  |                     TDC (24-bit)                       |

LR-TDC block
Sub-hearders
|  0xFC10  | "00" |  OerFlow  |  StopDuout  |  Through  |           # of word (11-bit)         |
|  0xFC20  | "00" |  OerFlow  |  StopDuout  |  Through  |           # of word (11-bit)         |
LR-TDC data
|   0xCC   |  "0"  |   Ch (7-bit)   |   "00000"   |                 TDC (11-bit)               |
```
データボディ領域ではsub-headerに続いてその領域のデータが返ってきます。

Mass trigger flagの内訳
<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">ビット番号</span></th>
    <th class="nowrap"><span class="mgr-10">フラグ</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">0</td>
    <td class="tcenter">No decision</td>
  </tr>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">Level2</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Fast clear</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">Consolation accept</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">Final clear</td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">Accept</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Clear</td>
  </tr>
  </tbody>
</table>

## Streaming TDC

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x11dc</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x05</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v3.5</td>
    <td class="tcenter">2021.4.</td>
    <td></td>
  </tr>
  </tbody>
</table>

**動作概要**<br>
このファームウェアはtrigger-less DAQ用に開発された外部トリガー無しに連続的に時間測定を行うTDCです。J-PARC MRの遅い取り出しの2秒間の間、1 nsの精度で時間測定を行うことを想定しています。Trigger-less DAQではフロントエンド回路上でトリガーによるイベント選別を行わないため、入力信号全てをデジタイズしPCへデータ転送を続けます。

HULはtrigger-less DAQにおいてdata streamingを行うにはデータリンクスピードが不十分であり、また十分なメモリ搭載していません。このファームウェアは連続時間測定の技術実証のために開発した側面が強いです。今後HULではこのファームウェアの開発は継続せず、**AMANEQ**というtrigger-less DAQ用に開発された回路に引き継ぐ予定です。Trigger-less DAQや連続読み出しに興味のある方は本多へ連絡をお願いいたします。

このような背景があるので、このファームウェアには試験的に実装した機能も存在します。ここでは汎用的に使えそうな部分のみ説明することにします。

**ブロック構造**<br>
入力はmain-inの上側を利用します。そのほか、テスト入力、VETO入力、スピルゲート入力をNIMINポートから行います。本TDCのチャンネル数は32 chです。テスト入力が有効の場合、main in Uの入力の代わりにNIMIN2からの信号が全チャンネルに配られます。高繰り返しのテスト信号を入れるとデータレートがすぐにリンクスピードを上回るので、利用時にはレートに気を付けてください。VETO入力はHIGHの間入力信号をマスクします。スピルゲートよりもさらに細かく入力信号の選択をしたい場合に利用してください。スピルゲートはJ-PARCの遅い取り出しのスピルゲートを想定しています。スピルゲートがHIGHの状態の時だけ、本TDCはデータを送信します。本TDCは端的にはスピルスタートからの時間を連続的に測るTDCです。そのため、スピルゲートは2秒間ONで2秒間OFFのように周期的にやってくることが前提になっています。

時間計測Online Data Processing (ODP) blockで行われます。ここで入力信号の立ち上がりと立下りの両エッジの時間を測定し、エッジペアを見つけます。この段階でTime-Over-Thresholdが計算され、立下りの時刻情報は破棄されます。以後、データワード内には立ち上がり時刻とTOT値が含まれます。

ODPブロックまでは各チャンネル毎に独立にデータ処理されます。Vital blockではデータパスを1つにまとめ上げ、データ列へハートビートデータという特殊データの挿入を行います。本TDCのコースカウントは125 MHzのクロックで駆動されている16-bitカウンタ (heartbeat generator)によって付与され、500 μs程度で1周します。それ以上の長さで時刻再構成を行うために、heartbeat methodという方式を導入しています。Heartbeat generatorが1周するごとにデータ列にheartbeatデータを挿入します。解析ではheartbeatデータの数を数えることで、スピルスタートからの時間を再構成することが出来ます。Heartbeatデータはデータ列のちょうど区切りの位置に挿入されなければならないため、vital blockにはtime frameの切れ目を見つける工夫が施されています。

データレートがSiTCPのスピードを超えた場合の対処など、さらに詳しい情報は[参考文献](https://doi.org/10.1093/ptep/ptab128)を参照してください。(R.Honda et al., PTEP, 2021)

![block-strtdc-fw](block-strtdc-fw.png "Streaming TDCのブロック図。"){: #STRTDCFW width=90% }

<table class="vmgr-table">
  <tr><td class="tcenter" colspan=2><b>Streaming TDC仕様</b></td></tr>
  <tr>
    <td>TDC精度</td>
    <td class="tcenter">0.97... ns</td>
  </tr>
  <tr>
    <td>最長スピルゲート長</td>
    <td class="tcenter">33秒</td>
  </tr>
  <tr>
    <td>最小パルス幅</td>
    <td class="tcenter">~4 ns</td>
  </tr>
  <tr>
    <td>最大パルス幅</td>
    <td class="tcenter">150 ns</td>
  </tr>
  <tr>
    <td>ダブルヒット分解能</td>
    <td class="tcenter">~7 ns</td>
  </tr>
  </tbody>
</table>

### レジスタマップ

TOT filerとtime-walk correctorはこのUGの中では説明していません。もし利用する場合は参考文献を読んでください。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x0)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>DAQデータ取得を開始するためのゲート。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Online Data Processing block: ODP (module ID = 0x1)</b></td></tr>
  <tr>
    <td>EnFilter</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>TOT filterの機能を有効にします。</td>
  </tr>
  <tr>
    <td>MinTh</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>TOT filterの下限閾値を設定します。この値よりTOTが小さいとフィルターされます。</td>
  </tr>
  <tr>
    <td>MaxTh</td>
    <td class="tcenter">0x1020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>TOT filterの上限閾値を設定します。この値よりTOTが大きいとフィルターされます。</td>
  </tr>
  <tr>
    <td>EnZeroThrough</td>
    <td class="tcenter">0x1030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>TOT値が0だったヒット（立下りエッジが見つからなかったヒット）をフィルターしないようにします。</td>
  </tr>
  <tr>
    <td>TwCorr0</td>
    <td class="tcenter">0x1040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域0の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr1</td>
    <td class="tcenter">0x1050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域1の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr2</td>
    <td class="tcenter">0x1060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域2の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr3</td>
    <td class="tcenter">0x1070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域3の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr4</td>
    <td class="tcenter">0x1080'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域4の補正値を設定します。</td>
  </tr>
  </tbody>
</table>

### HUL上のスイッチ・LEDの機能

**DIP SW2の機能**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">スイッチ番号</span></th>
    <th class="nowrap"><span class="mgr-30">機能</span></th>
    <th class="nowrap"><span class="mgr-10">詳細</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">SiTCP force default</td>
    <td>ONでSiTCPのデフォルトモードで起動します。電源投入前に設定している必要があります。</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Enable test in</td>
    <td>ONにするとNIMIN2が全チャンネルの入力に接続されます。</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">MCD mode bit-1</td>
    <td>MCDメザニンを搭載した際の動作を決めるビットです。通常はOFF (0)に設定します。<br>
    <ul>
      <li> 0b11: Master
      <li> 0b10: Repeater
      <li> 0b01: Slave
      <li> 0b00: Stand alone
    </ul>
    </td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">MCD mode bit-2</td>
    <td>MCDメザニンを搭載した際の動作を決めるビットです。通常はOFF (0)に設定します。</tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">Enable signal copy</td>
    <td>このビットがONだと、下側のメザニンコネクタへ入力信号のコピーが出力されます。別の回路でも信号を使いたい時に利用します。DTLメザニンカードを下側のメザニンスロットへ取り付けてください。</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
</tbody>
</table>

**LED点灯の機能**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">LED番号</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">LED1</td>
    <td>点灯中はTCP接続が張られています。</td>
  </tr>
  <tr>
    <td class="tcenter">LED2</td>
    <td>機能していません。</td>
  </tr>
  <tr>
    <td class="tcenter">LED3</td>
    <td>点灯中はスピルゲート入力がHIGHであることを示します。</td>
  </tr>
  <tr>
    <td class="tcenter">LED4</td>
    <td>機能していません。</td>
  </tr>
</tbody>
</table>


**NIM-IOへの信号アサイン**<br>
Streaming TDCにはIOMが存在しないため、入出力のアサインを変える事はできません。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">NIMIO</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=2><b>NIM-IN</b></td></tr>
  <tr>
    <td class="tcenter">NIN-IN1</td>
    <td>スピルゲート入力。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-IN2</td>
    <td>テスト入力。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-IN3</td>
    <td>VETO入力。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-IN4</td>
    <td>未使用</td>
  </tr>
<tr><td class="tcenter" colspan=2><b>NIM-OUT</b></td></tr>
  <tr>
    <td class="tcenter">NIM-OUT1</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-OUT2</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-OUT3</td>
    <td>NIM-IN1に入力されたスピルゲートのコピー出力。クロック同期を取った後の信号のため立ち上がりタイミングは量子化されている。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-OUT4</td>
    <td>未使用</td>
  </tr>
</tbody>
</table>

### DAQの動作

Streaming TDCはtirgger-less DAQで使用することを想定しているため、データ取得のトリガーという概念は存在しません。
データ転送を行う条件が揃うとFPGA即座にネットワークにデータを送信しようと試みるため、PC側は先にデータ準備が出来ていないといけません。
FPGAがデータを出力する条件は次の3つが全て成立している事です。
1. TCP connectionが成立している。
2. DAQ gateがONになっている。
3. Spill gateがON (NIMIN1の信号がHIGHである)。
本FWを使用する際には、上記順番の通りに処理することをお勧めします。
DAQ gateがONでspill gateがONになるとODPブロックはデータ計測を開始します。
この時点でTCP接続が確立していないと内部バッファにデータが溜まり続けていくため、バッファがあふれてしまう可能性があります。
TCP接続を確立してからRBCPでDAQ gateをONにしてください。

**データ構造**<br>
<span class=myred>Streaming TDCの1ワードは40-bitです。</span>受け取ったデータをそのまま`int32_t`や`int64_t`にキャストできないので、ソフトウェアの記述は工夫してください。
本FWにはspill start, TDC data, heartbeat, busy, spill endの5種類のデータが存在します。
それぞれ先頭の4ビットで識別が可能です。
Heartbeatデータはheartbeat (time) frameの境目に挿入され、spill startから何回目のheartbeatであるかを示すカウンターが下位16-bitに埋め込まれています。
もしvital blockの状態がbusyモードの場合にはheartbeatデータの代わりにbusyデータが返ってきます。

```
Spill start word
MSB                                                                                          LSB
|      0x1      |           RSV (20-bit)              |                  0xFFFF                |

TDC data
|      0xD      | '0' | TOT(8-bit) | Type(2-bit) | Ch(6-bit) |            TDC(19-bit)          |

Heartbeat data
|      0xF      |           RSV (20-bit)              |         heartbeat number (16-bit)      |
Busy data
|      0xE      |           RSV (20-bit)              |         heartbeat number (16-bit)      |

Spill end data
|      0x4      |           RSV (20-bit)              |                  0xFFFF                |
```

TDC dataに含まれるtypeは現状きちんと機能していません。
通常のTDC dataであれば`00`です。そのほかに`01`と`10`のパターンがありますが、これらであった場合そのデータは無視してください。

**Busyモード**<br>
FPGAが送信しようとするデータ量がデータリンクのスピードを超えると転送が追い付かなくなり、FPGA内部のバッファが溢れます。
このような状況にFWが陥ると、vital blockは通常の動作モードからbusyモードへ状態を遷移させ復帰を試みます。
Streaming TDCではtriggerを止めるためのBUSY信号は存在しませんが、busyモードではbusyデータがheatbeatデータの代わりに現れるようになります。
Busyデータが返ってきたheartbeatフレームではデータ欠落が起きていますが、どのデータをどれだけ落としたかは分かりません。
一部または全てのデータが欠落しているということだけが分かります。
一旦busyモードに移行すると最低3フレーム復帰するために必要とします。
更に詳しい動作については[参考文献](https://doi.org/10.1093/ptep/ptab128)を参照してください。

**データの並び**<br>
Streaming TDCから出力されるデータの並びを時間軸上に表した物を[図](#STRTDCDATA)に示します。
Spill gateの立ち上がりを検出するとまずspill start dataが出力されます。
次に最初のheatbeat frame内のTDC dataが続き、frameの境目にheartbeat (busy) dataが挿入されます。
Busy dataが返ってきた場合該当frameではデータ欠落が起きています。

![data-order-strtdc](data-order-strtdc.png "Streaming TDCから出力されるデータの並び順"){: #STRTDCDATA width=90% }