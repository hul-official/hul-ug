# Hardware

## HUL controller module

Hadron Universal Logic (HUL) controller moduleはVME 6U規格の回路基板であり、HULと呼ぶ場合このモジュールを指します。購入の際には以下の管理番号で有限会社ジー・エヌ・ディーへ問い合わせてください。
**ジーエヌディー管理番号：GN-1573-1**

### 仕様詳細

* 入力ポート
    * 差動入力64ペア (KEL 8831Eコネクタ)
    * LVDS、ECL、PECL、LVPECL等サポート
    * NIM入力 4ポート
* 出力ポート
    * NIM出力 4ポート
* メザニンスロット
    * 2スロット
    * 双方向LVDS 32ペアにてFPGAと直接接続
    * メザニン電源: HUL controllerから+3.3V給電
* 通信インターフェース
    * RJ45: GbE (1000BASE-T)
    * VME J1は通信機能を有さない
* FPGA
    * Xilinx Kintex7 (XC7K160T-1FBG676C)
* コンフィギュレーションメモリ記録媒体
    * SPI flash 3.3V用 (製造時期に依存し以下3種のいずれか)
      * N25Q128A13EF840E
      * MT25QL512ABB1EW9-0SIT
      * S25FL256SAGNFI001
    * SPI (シリアル) コンフィギュレーションモード
* 発振器 (クロックソース)
    * 50 MHz LVCMOS (~50 ppm)
      * Peak-To-Peak jitter 30ps
* トリガーバス
    * KEK-VME J0
    * ドライバ機能とレシーバ機能の選択可能
* 電源
    * DC +5V
    * AC/DCアダプタ, もしくはVME-J1コネクタから給電
* 消費電力
    * 静的: 0.5~0.7 W @3.3 V (主にPHY)、0.5~0.7 W @1.0 V (主にFPGAコア)
    * 動的: FPGA firmwareに強く依存

HULの写真とブロック図を以下に示します。

![hul-photo](hul-photo.png "HUL controllerの写真、およびブロック図。"){: style="display:block;  margin:auto" width=100% }

Main port U (D)
  : フロントの固定入力ポートです。コネクタはハーフピッチの68極コネクタ（KEL 8831E-068-170L-F）です。適合するコネクタでケーブルを作成して接続してください。チャンネルアサインはUおよびDに対してそれぞれ0-31、 32-63 chであり、基準マーカー側が若い番号になります。GND接続はA1A2、および B1B2の2ペア (基準マーカー直下) です。コモンモードレンジで－4Vから+5Vまでの差動信号をサポートします。LVDS、ECL、PECL、LVPECLなどがサポート対象です。ECLおよびPECL信号規格を利用する場合、HUL側に電流制御用の抵抗がない事に注意してください。ドライバ側で電流制御されている必要があります。
  : 固定入力ポートでは差動信号をLVCMOSに変換してからFPGAへ入力します。最高繰り返しスピードは560 Mbpsです。それよりも高速な信号を入力する場合メザニンカードの使用を検討してください。ワイヤーチェンバーなどのASD出力を入力するような用途ではこの制限は問題になりません。

NIM IN
  : NIMレベルの4入力ポートです。チャンネルアサインは基板上シルクを参照してください。**ポート番号は1から始まります**

NIM OUT
  : NIMレベルの4出力ポートです。チャンネルアサインは基板上シルクを参照してください。**ポート番号は1から始まります**

LAN
  : RJ45のイーサネットコネクタです。Gigabit Ethernet PHYが接続されています。SiTCPを通じてFPGAとPCが通信するために利用します。

Mezzanine slot U(D)
  : 上側のメザニンカードの取り付けスロットです。1つのスロットで2つのMOLEX-071439-0464コネクタを利用します。2つのコネクタのうち上側が信号線、下側に電源とGNDがアサインされています。適合するメザニン側のコネクタはかみ合わせの高さにより複数存在しますが、MOLEX-071436-1464を採用した場合9 mmの足+0.5mmのワッシャーで支えると最も安定します。
  : 信号線は32ペアの差動線でFPGAとつながっています。メザニンベースコネクタまでは信号方向が定まっていないため、双方向LVDSが利用できます。実際にどのように入出力を行うかはメザニンの設計とFPGA側の設計に依存します。スロットUおよびDに入力ポートを割り当てた場合、メインポートから数えてチャンネルアサインはそれぞれ64-95および96-127 chになります。メザニンベースコネクタとFPGA間の差動配線はパターンレイアウトの都合でいくつかp/nが反転しています。反転している箇所にインバータを挿入してユーザーが使える状態にするためにはMZN_NetAssign.vhdを通す必要があります。
  : メザニンベースへの供給電源はHUL controllerからの+3.3 Vです。HUL controller上の+3.3 V電源容量は6 Aであり、HUL上で1-2 A程度消費しているため、猶予は4-5 A (13-16 W) 程度です。

CN14
  : JTAGダウンロードケーブル用のコネクタです。XilinxのUSBダウンローダーに対応しています。bitstreamやMCSのダウンロード方法については7章で述べます。

SW1
  : VME J0バスの入出力方向を決めるdip SWです。HUL controllerがJ0バスドライバになる場合、1-8番すべてをONにします。 レシーバ状態 (Default) にする場合、1-8番全てをOFFにします。1つのクレートに2つドライバが存在するとショートします。

SW2
  : ユーザー用のdip SWです。各スイッチの役割はFPGA firmwareに依存します。

SW3
  : ユーザー用のスイッチです。押すと一定幅のパルスをFPGAに送ります。役割はFPGA firmwareに依存しますが著者（本多）が開発したFWでは原則最も上位のリセット信号です。詳しくは後述します。押した際のレベル遷移は1→0です。(負論理)

JP1
  : <span class="myred">利用しません。開放にしてください。</span>

JP2, 3
  : VME J1から給電を受ける場合両方をショートさせます。HUL controllerの一次電源はDC +5Vです。

JP4, 5
  : AC/DCアダプタから給電を受ける場合両方をショートさせます。

AC/DC
  : 一般的なプラグ（外径：5.5mmΦ、内径：2.1mmΦ）のAC/DCアダプタの接続口です。

FUSE
  : 容量5AのFUSEです。替えのFUSEは例えばリテルヒューズのPICO® ヒューズを購入して使ってください。

VME J1
  : VMEクレートのJ1コネクタです。電源の+5Vラインのみが接続されています。

VME J0
  : KEK-VMEクレートのJ0バスコネクタです。J0バスの電源へは接続していません。**標準装備ですが取り外すこともできます。外してもJ0バスが使えなくなるだけで機能に違いはありません。**必要ない場合は注文時にCN9を外すように指示してください。

## HUL Mezzanine cards

### HUL Mezzanine Drift Chamber Receiver (DCR) Ver.1

Drift Chamber Receiver (DCR) Ver.1はドリフトチェンバー用のAmp Shaper Discriminator (ASD) カード (ジーエヌディー管理番号：GNA200) の出力信号をバッファして、FPGAへ入力するための信号入力用回路基板です。機能的には32 ch入力のLVDS信号リピート基板が正しい呼称になります。Ver2と違い全経路で差動信号を維持し、かつLVDSリピータICの性能もよいため、繰り返しスピードの点でVer2よりも有利です。同様の理由でHUL controllerの入力ポートよりも有利です。ただし微妙な差であるためサポート信号の種類が多いver.2が上位互換です。
**ジーエヌディー管理番号：GN-1573-S1**

![DCRV1](mzndcr-v1.png "メザニンDCR v1"){: style="display:block;  margin:auto" width=50% }

Input Port
  : LVDS入力ポートです。コネクタはハーフピッチの68極コネクタ（KEL 8831E-068-170L-F）です。適合するコネクタでケーブルを作成して接続してください。HUL controllerの入力ポート同様、基準マーカー側が若い番号になります。GND接続はA1A2、および B1B2の2ペア (基準マーカー直下) です。DCRはv1、v2共に基板の配線パターンの都合で、見た目上のチャンネルの並びから配線パターンがスワップされています。また、HUL controller上の配線のp/n反転箇所も考慮に入れなければいけません。そのため、FPGA firmwareでネットアサインを見た目の番号どおり並べ替えるためには、[下図](#DCRNET)に示すようにDCR_NetAssign.vhdを通す必要があります。

![DCRNET](dcrnet.png "DCRのネットスワップの概念図"){: #DCRNET width=50% }

LVDSリピータ
  : LVDSを受信してそのままリピートするICです。FPGA保護のために一度信号を中継します。

### HUL Mezzanine Drift Chamber Receiver (DCR) Ver.2

Drift Chamber Receiver (DCR) Ver.2は機能的にはver.1と同様ですが、差動信号レシーバICにHUL controllerと同じICを用いているため、様々な差動信号を受信することができる回路基板です。HULの入力ポート拡張用途であれば、特別な理由がない限りver.2の方を購入してください。
**ジーエヌディー管理番号：GN-1626-1**

![DCRV2](mzndcr-v2.png "メザニンDCR v2"){: width=50% }

Input Port
  : 機械的にはDCR Ver.1と同様です。電気的にはコモンモードで－4Vから+5Vまでの差動信号入力をサポートします。Main portで使われているICと同じものを用いて差動信号を一度LVCOMSへ変換します。その後LVDSへ再変換してFPGAへ入力します。DCR ver.1と同様に、FPGA firmwareでネットアサインを並び替えるために、DCR_NetAssign.vhdが必要です。

### HUL Receiver Module (HRM)

Receiver moduleはJ-PARCハドロン実験等で利用されている Master Triger Module (MTM: GNN-570) によって配布されたトリガー信号やイベント番号を受信し、MTMへBUSY信号などを返送する回路基板になります。HRMはCat5eなどのツイストペアケーブルで送られてくるトリガー信号やイベント番号をデシリアライズし、ユーザー (FPGA) が扱いやすいバス信号形式変換してFPGAへ信号を渡します。また、FPGAから送られてくるBUSYとRSV2をツイストペアケーブルでMTMへ送信するために信号変換します。HRMが受信したトリガー信号やHRMへ送信するBUSY信号などの処理はすべてFPGAで行います。本回路を取り付けることで、HUL cotrollerはKEK-VMEクレートのJ0バスコントローラとして機能するようになります。
**ジーエヌディー管理番号：GN-1627-1**

![HRM](mznhrm.png "HUL receiver module (HRM)"){: width=50% }

Port A
  : トリガー入力ポートAです。MTMのport A、もしくはリピータのPort Aとツイストペアケーブルで接続します。

Port B
  : トリガー入力ポートBです。MTMのport B、もしくはリピータのPort Bとツイストペアケーブルで接続します。

LED (LOCK)
  : Port Bから送られてくるイベントタグビットからクロックが正しく復元できている事を示すPLL LOCKビットの状態を示します。PLL LOCKがHIGHだと緑のLEDが点灯します。

LED (BUSY)
  : MTMへ送信するBUSY信号の状態を示します。BUSYがHIGHであれば赤のLEDが点灯します。HRMは直接クレートバスに繋がっていないため、HUL controllerのFPGAから正しくBUSYが送られてきている必要があります。

JP1
  : デシリアライズされたタグ情報はデコーダICから出力されるRCLKクロックに同期してバスに現れます。JP1はバス信号がRCLKの立ち上がりに同期するか、立下りに同期するかを決めるジャンパです。デフォルトはRです。

**Mezzanine HRMへの入出力信号**

| 信号名 | 入出力方向 | コメント |
| ------ | --------- | -------- |
| RCLK   | IN        | 受信したイベントタグ情報から復元したクロックです。|
| LEVEL1 | IN        | Level1 trigger信号です。 |
| LEVEL2 | IN        | Level2 trigger信号です。 |
| Clear  | IN        | Fast clear信号です。 |
| RSV1   | IN        | MTMから送られてくるReserve 1信号です。|
| LOCK   | IN        | PLLのlockビットです。受信したタグからクロックが正しく復元できていることを示します。 |
| SNINC  | IN        | Spill Number Incrementです。この信号を使ってFPGA内部でスピル数を数えることが可能です。 |
| Event counter | IN | MTMから送られてくる12ビットのイベント番号です。 |
| Spill counter | IN | MTMから送られてくる8ビットのスピル番号です。 |
| RRFB   | OUT       | タグ情報が出力されるタイミングを決めます。HighだとRCLKの立ち上り、lowだと立下りで出力されます。基板上のJP1の設定が優先されます。 |
| BUSY   | OUT       | MTMへ送るBUSY信号です。 |
| RSV2   | OUT       | MTMへ送るReserve 2信号です。|

### HUL Mezzanine differential signal transmitter LVDS (DTL)

Mezzanine DTLはHUL本体のFPGAからメザニンカードを通じて外部へ信号を出力するための拡張カードです。回路的には殆どDCRv1と同一であり、LVDSリピータの入出力の向きが逆になった回路です。HUL本体との信号接続の並びはDCRv1と同じです。HULからはLVDSで信号を出力してください。
**ジーエヌディー管理番号：GN-1724-1**

![DTL](mzndtl.png "Mezzanine DTL"){: width=50% }

Output Port
  : LVDS出力ポートです。信号の並びやGNDの位置等、機械的にはDCR v1と同一です。また、本メザニンを使用する際にもDCR_NetAssign.vhdを通す必要がありますが、DCRv1の場合と入出力の向きが逆になります。

### HUL Mezzanine NIM extension (NIM-Ex)

Mezzanine NIM-ExはHUL controllerに備わっているNIM入出力を拡張するための回路です。LEMOコネクタが合計12 ch備わっており、2 chずつ入出力の方向をスイッチで切り替えることが出来ます。HUL controllerのNIM IOと同様のICを採用しており、同等の速度の入出力が行えます。

<span class="myred">NIM-Exあたりの消費電力は5.5Wと大きいです。</span> メザニンカード上で-3.3Vを生成しており基板自体が熱くなるため、空冷できない状況での使用は控えてください。熱により壊す可能性が十分にあります。

<span class="myred">回路図に間違いが多くあります。</span> 回路図ネットから正しいXDCとHDLコードを推測することが難しいため、参考用にskeletonプロジェクトにNIM-Ex用のexample designを同梱しました。

![NIMEX](mznnimex.png "Mezzanine NIM-Ex"){: width=50% }

LEMO
  : LEMOコネクタ12 chです。Example designで提供されているNimEx_NetAssign.vhdにおけるネット番号とは[上図](#NIMEX)のように対応します。

Dir SW
  : 信号入出力方向を2 ch毎に設定するためのスイッチです。LEMOコネクタ側に倒すと出力、HUL側へ倒すと入力となります。

### HUL Mezzanine High-resolution TDC

Mezzanine HR-TDCは~30 psの精度で時間を測定するFPGA high-resolution TDCです。TDCの種別としては、common stop型のhigh-resolution-multi-hit TDCとなります。メザニンカード上に時間計測専用のFPGAを持ち、HUL本体からの制御とデータ吸出しを必要とします。本メザニンはトリガー入力により時間を測定し決まったフォーマットでHUL側のFPGAへデータ転送する機能しか持たないため、HUL側のFPGAの記述によっては単純なTDCのみならず、TOF triggerなど他の方法で利用することが可能です。詳しい動作や制御方法は第4章で述べます。

本メザニン上のFPGAはHUL本体と同じKintex-7 160T-1です。メザニン1台で32 ch分の時間測定（leading/trailing両エッジ）が可能です。入力はLVDSのみであり、ECLはサポートしていません。FPGA用のクロックは基板上の発振器とHULからのクロックが選択可能です。電源はHUL本体からの3.3Vによって賄われ、本メザニン上で必要な電位は全てLDOで生成されます。メザニンあたりの消費電力は5W程度です。HUL本体上の3.3V電源がLMZ30604RKGT（4A電源）の場合、2スロットに本メザニンを挿すと電流不足で正しく動作しません。必ずLMZ30606RKGT（6A電源）のHUL基板を利用してください。また、2スロット搭載時はシステム全体でおよそ20 W電力を消費します。発熱が大きいので必ず空冷してください。熱を持つとFPGAのリーク電流が爆発的に増えて、予期せぬトラブルを起こすと思われます。クレートに挿して利用することを強くお勧めします。（グランドが安定しない環境では性能が出ません。）
**ジーエヌディー管理番号：GN-1644-1**

![MZNHRTDC](mznhrtdc.png "Mezzanine HR-TDC"){: width=50% }

Input Port
  : 信号入力ポートです。LVDS規格のみサポートします。コネクタはハーフピッチの68極コネクタ（KEL 8831E-068-170L-F）です。GND接続はA1A2、および B1B2の2ペア (基準マーカー直下) です。チャンネル番号は基準マーカー側から見た目通り0から31までです、チャンネルのスワップはありません。

CN4
  : Xilinxダウンロードケーブル用のコネクタです。FPGAとSPI flash memoryのアクセスが可能です。本メザニンのSPI flashは1.8V用のN25Q128A11EF840E、もしくはMT25QU256ABA1EW9-0SITです。HUL本体のICとは異なります。

SW1
  : 基板上のFPGAを制御するためのdip SWです。現状ではFPGAが利用するクロックを基板上の発振器かHULからのクロックにするかを選ぶことが出来ます。

X1
  : 100 MHzの発振器です。HULからクロックを送信する場合、周波数は同じである必要があります。

**Flash memoryについて**<br>
Mezzanine HR-TDCで使用しているflash memoryには以下の2つのケースがあります。買った時期によって異なり、どのflash memoryが搭載されているかはVivadoを使わないと調べられないです。シールなどで見分けられるようにしてください。

<ul>
  <li> N25Q128A11EF840E (初期版)
  <li> MT25QU256ABA1EW9-0SIT (2020年度製造版より)
</ul>