# Practical Usage

この章では多分使っていく上で知らないと困ることを何点かあげます。

## Vivadoを使ったMCSの生成とダウンロード

**MCSの生成**<br>
Vivadoを使ったMCSの生成方法を説明します。iMpactは持っていない人もいると思うので省略します。Vivadoでプロジェクトを開き、tool → Generate Memory Configuration Fileを選択します。出てきた画面を[図](#GENMCS)様に埋めてOKを押すとMCSが生成されます。この画面はVivado 2020.1のものです。

HULには2021年現在3種類のSPIフラッシュメモリが使われています。それぞれVivadoでは以下のパーツを選択してください。
<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">基板上の部品</span></th>
    <th class="nowrap"><span class="mgr-20">メーカー</span></th>
    <th class="nowrap"><span class="mgr-20">Vivadoで選択するパーツ</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>N25Q128A</td>
    <td>Micron</td>
    <td>mt25ql128-spi-x1_x2_x4</td>
  </tr>
  <tr>
    <td>MT25QL512</td>
    <td>Micron</td>
    <td>mt25ql512-spi-x1_x2_x4</td>
  </tr>
  <tr>
    <td>S25FL256S</td>
    <td>Spansion</td>
    <td>s25fl256sxxxxxx0-spi-x1_x2_x4</td>
  </tr>
  </tbody>
</table>

![gen-mcs](genmcs.png "MCS生成画面"){: #GENMCS width=90% }

**Hardware managerでMCSをダウンロードする**<br>
HULに電源が入った状態でJTAGケーブルを接続します。この時Xilinx純正であればLEDが緑色になります。仮想マシンなど立ち上がっているとクライアントOSにUSB接続が行くことがあるのでオレンジのままならそれらであることが多いです。VivadoでOpen Hardware manager → Open targetでFPGAにアクセスします。この段階でKintex7 (XC7K160T-1)が見えていると思います。Bit streamファイルを書き込むならこの段階でFPGAを右クリックしてBit streamファイルをアサイン、書き込みを行えばOKです。MCSの場合SPIフラッシュメモリにダウンロードするのですが、この時点では画面上に出てきていません。これはSPIフラッシュメモリがJTAGチェイン上に存在しないためです。

まずconfiguration memory deviceを追加します。FPGAを右クリックし、Add configuration memory deviceを選択します。[図](#DLMCS)のような画面が出るのでMemory DeviceにMCSを作った際に選択したメモリを選択します。その下に書き込むMCSとPRM両方を選択して、他は全てデフォルトにしておいてください。後は書き込みを行うだけです。大体書き込むのに10分くらいかかります。

![dlmcs](DLMCS.png "Configuration Memory Device追加画面"){: #DLMCS width=60% }

**N25Q128Aの取り扱いについて**<br>
Micronのn25q128aはHULを設計した時に採用した石ですが、生産終了となっています。そのため、すでに最新版のVivadoでは正式サポートから外れています。後継版の石であるMT25系と内部的には互換らしいので、N25用のMCSを生成する際にはMT25QL128を選択してMT25QLとして作ってください。また、ダウンロードする際には同じようにMT25QLとしてダウンロードしてください。

**SPI flashをMT25QL512に変更した基板について**<br>
2017年後期の共同購入時、SPI flash memoryがMT25QLに変更になっています。ソースコードから合成してMCSファイルを作る場合、[図](#GENMCS)に書かれているSPIメモリからmt25ql512_spi-x1_x2_x4変更してください。同様にHardware managerでダウンロードする場合も[図](#DLMCS)に示すmemory deviceからmt25ql512-spi-x1_x2_x4へ変更してください。

**SPI flashをS25FL256SAGNFI001に変更した基板について**<br>
2018年度から購入可能であったHULにはSpansionのS25FL256SAGNFI0が搭載されています。ソースコードから合成してMCSファイルを作る場合、s25fl256sxxxxxxx1-spi-x1_x2_x4を選択してください。同様にHardware managerでダウンロードする場合もs25fl256sxxxxxxx1-spi-x1_x2_x4へ変更してください。

**JTAGケーブルをつないでHardware managerを立ち上げたままHULに電源を入れるとFPGAの初期化が終わらないことがある**<br>
Hardware managerが起動した状態、かつJTAGサーバがOpenの状態でダウンロードケーブルをつないだまま電源を投入するとこのトラブルに遭遇します。Hardware managerが立ち上がっていてもサーバをCloseしていれば回避できます。

### Mezzanine HR-TDCへのMCSダウンロード

Hardwareの章で記述した通りmezzanine HR-TDCでは2021年現在2種類のflash memoryのパターンがあります。ダウンロードの際には以下のパーツを選択してください。
<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">基板上の部品</span></th>
    <th class="nowrap"><span class="mgr-20">メーカー</span></th>
    <th class="nowrap"><span class="mgr-20">Vivadoで選択するパーツ</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>N25Q128A</td>
    <td>Micron</td>
    <td>mt25qu128-spi-x1_x2_x4</td>
  </tr>
  <tr>
    <td>MT25QU256A</td>
    <td>Micron</td>
    <td>mt25qu256-spi-x1_x2_x4</td>
  </tr>
  </tbody>
</table>

## FMP用いたSiTCP経由のMCSダウンロード

Flash Memory Programmerを使う事でネットワーク（RBCP）経由でSPIフラッシュメモリへMCSをダウンロード出来ます。JTAGを利用する場合USBダウンロードケーブルを基板に挿す必要があるため、作業者が1つ1つ基板の近くで作業する必要があります。実験中にFWを入れ替えようと思った場合大がかりな作業となります。FMPを利用するとモジュールに触らずして計測室からMCSのダウンロード、およびFPGAの再コンフィグが可能となり、リモートでメンテナンスが出来ます。

MCSファイルはJTAGで書き込む場合と同じものを利用してください。SPIフラッシュメモリに書き込むための実行体は`hul_software.git/Common/bin`にあります。以下のように実行してください。もし、`mcs_converter`で変換済みのバイナリファイルがある場合は`-b`オプションを付けて実行してください。
```
./bin/flash_memory_progorammer [ip-address] [mcs file path]
```
ダウンロードが成功し、ベリファイの結果が元のMCSと一致すると以下のような表示が出ます。もし、途中で通信が途絶してしまったり、mismatchと表示された場合は成功するまでリトライしてください。この段階ではSPIフラッシュに書いただけでFPGAには反映されていません。正常に書き込めた後`reconfig_fpga`を使ってFPGAを再コンフィグしてください。

![fmp](fmp-download.png "FMPを通じてMCSをダウンロードした際のコンソール表示。図は書き込みが成功した例。"){: #FMP width=90% }

**既知の問題点**<br>
フラッシュメモリのページ書き込みにはそれなりの時間がかかり、PCのマシンスペックや通信環境次第ではUDP RBCPが1サイクル回るまでの間にページ書き込みが終わりません。現在のFMPはページ書き込み中に次の書き込み動作をブロックするように設計されておらず、次のページ書き込みのリクエストが早すぎると通信が途絶したりUDP RBCPのバスエラーが発生したりします。現状`usleep`で強制的に次の書き込みを遅らせていますが、良い解決方法ではないし書き込みかかる時間も長くなります。将来的にはFPGA側で対応できるように改良したいですが、まだめどはたっていません。

## 使用時のハウツーなど

**簡単なテスト方法**<br>
HUL RM、HUL Scaler、HUL MH-TDCの簡単なテスト方法を述べます。これらのファームウェアはVMEクレートに挿さずに動かしてもとりあえず動きます。`daq_func.cc`で`TRM::SelectTrig`に設定するレジスタを`TRM::L1Ext`のみにします。その状態でNIMIN 1へトリガー信号をつなぎます。とりあえずデータが返ってくる様子が見たい場合はこの状態で`./bin/daq`を実行すればデータが返ってくるはずです。

**クレートにたくさん挿して使用する方法**<br>
クレートに複数のHULを挿す場合J0 bus masterが1台必要になります。逆にその他は正しくslaveである必要があります。Masterが2台あるとJ0がショートします。（KEK VMEクレートを使わない場合はこの限りではありません。）この説明ではL2を利用するか？L1やL2はどこから入力するか？は考慮の外に置いています。

**Masterの設定方法**<br>
HRMをSlot-Uにマウントする。
- DIP SW1を全てONにする。（これがONだとJ0 busに対してドライバモードになります。）
- DIP SW2の2 bit目（mezzanine HRM）と4 bit目（Bus BUSY）をONにする。（ファームウェアがHRMをサポートしている必要があります。）
- `TRM::EnRM`を立てる。（EnJ0は立てない）

**Slaveの設定方法**<br>
- DIP SW1を全てOFFにする。
- `TRM::EnJ0`を立てる。（EnRMは立てない）
この状態でSlave側のforce busy (DIP SW2 3 bit目)をONしてHRMのbusy LEDが光れば正しくつながっています。

**KEK VMEクレートJ0からLevel2を受け取る場合、モジュールリセットで問題が発生する**<br>
モジュールリセット (BCTリセット等) をかけた後DAQが回らないという現象が既知の問題として知られていましたが、version 3系で解決したと思われます。同様の問題が発生したら報告してください。

**イベントスリップが発生した場合**<br>
J0やHRMのタグを使ってイベントスリップを監視することが出来ます。これらがモジュール間で一致しない場合、確実にイベントが滑っています。HULのファームウェアはmulti-event bufferを持っている都合上、一度イベントが滑るとRUNのStart/Stopでは解決しません。BCT::Resetを呼び出して内部のバッファをすべてクリアする必要があります。

**HR-TDCの使い方**<br>
HR-TDCはFPGAが2つあり、なおかつ初期化手順があるため使い方を間違えるとすぐにハングアップして動かなくなります。最初はここに書いた方法で動かしてみることをお勧めします。**必ずVMEクレートに挿してテストしてください。**

1. HULへMezzanine HR-TDCを挿してそれぞれのFPGAにMCSをダウンロードします。電源投入後にbit streamで入れるとBCTがハングすることがあるので、MCSから始めることをお勧めします。Mezzanine HR-TDCにMCSをダウンロードする際に指定するmemory deviceはmt25qu128-spi-x1_x2_x4です。
2. 一度クレート電源をOn/Offします。
3. ソフトウェアの準備をします。Mezzanine HR-TDCが2枚とも刺さっている場合は配布したC++コードがそのまま使えます。もし片方がだけだけ刺さっている場合、RegisterMap.hhの`EnSlotUp/EnSlotDown`をfalseにしてください。
4. `./bin/debug`を実行します。HUL本体のバージョン、メザニンのバージョンが表示されます。ここでバスエラーが出る場合何かがおかしいです。まずソフトを確認し、間違いがなければメザニンとの接続が疑われるので一度付けなおしてください。
5. `./bin/initialize`実行しDDRレシーバの初期化、校正クロックによる校正とestimatorの生成を行ってください。両方が成功していることを確認してください。
6. 配布したソースコードはNIM-IN1にコメンストップを入力することを想定しています。 NIM-IN1へトリガーを接続して、`./bin/daq [run no] [# of events]`を実行してください。。 データファイルは`./data/run[run no].dat.`の様に生成されます。
7. `./bin/decode [run no]`実行しデコードの実効とROOT fileの生成を行います。ROOT fileは`./rootfile/run[run no].root`のように生成されます。

## SiTCPのトラブル

### TCP接続を切断した後再接続する際に1分ほど待たされる

EEPROMの中身が不正な値で埋まっていると起こる現象です。
SiTCP UtilityでEEPROMへアクセスすると[図](#SITCP-TROUBLE) (a)のように全てのパラメータが非常に大きな値になっていると思います。
この場合、EEPROMの初期化を行うことで対処することが可能です。
hul_software内のerase_eepromを使ってEEPROMの中身を一度すべて消去してください。
```
./bin/erase_eeprom [ip address]
```
SiTCPのライセンスやIPアドレスを含めて全ての情報が消えるので、再設定してください。
再設定を行ったら一度電源を落として、再度立ち上げてください。
SiTCP UtilityでEEPROMへアクセスし、[図](#SITCP-TROUBLE) (b)のようなパラメータが返ってきたら修復完了です。
うまくいかない場合、EEPROMを消去した後電源を落として、DIPの設定をデフォルトIPモードへ変更してから再設定してみてください。

![BCT](sitcp-trouble1.png "Screenshot of SiTCP Utility. (a) the case for SiTCP, which has a trouble. (b) normal condition."){: #SITCP-TROUBLE width="80%"}

## その他

**ジー・エヌ・ディーから購入したい**<br>
有限会社ジー・エヌ・ディーにメールを送ってジーエヌディー管理番号のモジュールを何台購入するか伝えてください。在庫があれば即納してもらえます。HUL controllerではればSiTCPライセンスは価格に含まれています。
在庫がない場合どのように製造するかはその都度相談になります。






