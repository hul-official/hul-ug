# SiTCP

SiTCPはKEKの内田さんによって開発された技術で、本来CPUが必要なTCP/IP通信をハードウェアのみで実現した技術です。詳しい使い方や最新のIPコアは内田さんのweb siteやBBTで入手してください。

* [内田さんのweb site](https://www.sitcp.net/)
* [BBTのweb site](https://www.bbtech.co.jp/products/sitcp-library/)


SiTCP動作の概略は[下図](#SITCPBLOCK)のようになります。SiTCPは起動時にPHYを初期化して、EEPROMからMACアドレスとIP addressをロードします。Force defaultで起動するとSiTCPが持つDefault IP address (192.168.10.16) とMACアドレスがロードされます。Default状態では他の機器が存在するネットワーク空間では使えないのでPCと一対一で使うことになります。FPGA内部の回路へ提供する通信機能はTCPとUDPになります。TCPは100 Mbspと1 Gbpsの両方の通信をサポートし、PHYからの信号により自動で切り替わるように設定することが可能ですが、現状のHULのファームウェアでは1 Gbps固定です。TCP通信はシステムクロックに同期した8-bit送信と8-bit受信を行うことができますが、TCPによるデータ受信は推奨されていません。レジスタの設定などはUDP通信を使うことが望ましいです。UDP通信はRBCPと言う独自のパケットを用いて制御コマンドの送付、アドレス送付、データ送受信を行う機能を提供します。RBCPはacknowledgeを返すことによってPCとSiTCPでハイドシェイク通信を行っています。UDP通信はシステムクロックに同期して32-bitアドレス送付、8-bitのデータ送受信を行います。また、特定のアドレスを指定することでSiTCPの内部レジスタやEEPROMに直接アクセスすることが可能ですが、特殊な要求がない限り変更する必要はないと思います。

SiTCPはデフォルトではkeep aliveしない設定となっているので、極端にトリガーレートが低い場合では（1 trigger/30min程度）keep alive packetの送信を行うように設定しないとコネクションが閉じてしまいます。また、直接SiTCPのレジスタにアクセスすることで後述するBBTのソフトウェアを用いずともIP addressの変更を行うこともできます。

![SiTCP](sitcp.png "SiTCP関連回路のブロック図"){: #SITCPBLOCK width="50%" }

## IP addressとMACの設定方法

IP addressとMACは基板上のEEPROMに格納されており、SiTCPを経由して書き込むことが前提となっています。そのため、最低でもSiTCPが動く状態のFWがFPGAにダウンロードされている必要があります。これらの書き込みはRBCP通信を使って自分で行うこともできますが、BBTの用意したツールを用いるのがいいでしょう。これらのツールは前述のBBTのサイトからユーザー登録をすることによって入手できます。

MACは“SiTCPMpcWrite”を用いて書き込みます。ツールを立ち上げたらジー・エヌ・ディーから納品されたDVDディスクにあるファイルを選択して書き込んでください。（MACは直接BBTから買うこともできますが、ジーエヌディーへ任せてください）MACは機器固有の物理アドレスのため、衝突させると動かなくなります。MPC書き込みツールについては[BBTのサイト](https://www.bbtech.co.jp/download-files/sitcp/index.html)を参照してください。

IP addressはSiTCP Utilityを用いて書き換えを行います。このツールはマルチプラットフォーム向け (β版) に用意されているようです。この作業はMACを入れた後行います。[下図](#SITCPUTIL)に示す赤枠で囲んだ「EEPROMにアクセスする」にチェックを忘れずに入れてください。次に制御対象のIP addressを入力して表示ボタンを押してください。（もし、IP addressがわからない場合force defaultで起動してください。）右上の目が動いて情報が表示されたら読めています。次に、書換情報のIP addressを設定したい値にして書き換えを押してください。この段階ではSiTCPのIPは変わっていません、EEPROMに格納しただけです。正しく書けているかどうかはもう一度読み出してみるとわかります。ここで設定したIPは次回電源投入時から有効です。

![SiTCPUtil](sitcp-util.png "SiTCP Utilityの操作画面"){: #SITCPUTIL width="50%" }